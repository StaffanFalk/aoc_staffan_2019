from unittest import TestCase

from day9.intcode_computer5 import IntCodeComputer5


class TestIntCodePuzzle1(TestCase):
    def test_it(self):
        self.check1([109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99],
                    [109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99])
        self.check3([1102,34915192,34915192,7,4,7,99,0], None, 16)
        self.check2([104,1125899906842624,99], None, 1125899906842624)
        # self.check1(intcodes, expected_result)
        # self.check2(intcodes, input, expected_output)

    def check1(self, intcodes, expected_result, input: int = None):
        ic = IntCodeComputer5.simple_evaluate(intcodes, input)
        self.assertEqual(expected_result, getattr(ic, "intcodes"))

    def check2(self, intcodes, input: int, expected_output: int):
        ic = IntCodeComputer5.simple_evaluate(intcodes, input)
        self.assertEqual(expected_output, ic.last_output)

    def check3(self, intcodes, input: int, expected_output: int):
        ic = IntCodeComputer5.simple_evaluate(intcodes, input)
        self.assertEqual(expected_output, len(str(ic.last_output)))
