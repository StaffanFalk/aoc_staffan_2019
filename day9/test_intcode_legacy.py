from unittest import TestCase

from day9.intcode_computer5 import IntCodeComputer5


class TestIntCodeLegacy(TestCase):
    def test_legacy_day2(self):
        self.check1([1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50], [3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50])
        self.check1([1, 0, 0, 0, 99], [2, 0, 0, 0, 99])
        self.check1([2, 3, 0, 3, 99], [2, 3, 0, 6, 99])
        self.check1([2, 4, 4, 5, 99, 0], [2, 4, 4, 5, 99, 9801])
        self.check1([1, 1, 1, 4, 99, 5, 6, 0, 99], [30, 1, 1, 4, 2, 5, 6, 0, 99])

    def test_legacy_day5(self):
        self.check2([3, 0, 4, 0, 99], 88, 88)
        self.check1([1002, 4, 3, 4, 33], [1002, 4, 3, 4, 99], 1)
        self.check1([1101, 100, -1, 4, 0], [1101, 100, -1, 4, 99], 1)
        self.check1([3, 7, 1, 7, 6, 7, 99, 0], [3, 7, 1, 7, 6, 7, 99, 100], 1)

        self.check2([3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8], 8, 1)
        self.check2([3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8], 5, 0)
        self.check2([3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8], 12, 0)
        self.check2([3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8], 7, 1)
        self.check2([3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8], 8, 0)
        self.check2([3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8], 9, 0)
        self.check2([3, 3, 1108, -1, 8, 3, 4, 3, 99], 6, 0)
        self.check2([3, 3, 1108, -1, 8, 3, 4, 3, 99], 8, 1)
        self.check2([3, 3, 1108, -1, 8, 3, 4, 3, 99], 88, 0)
        self.check2([3, 3, 1107, -1, 8, 3, 4, 3, 99], 5, 1)
        self.check2([3, 3, 1107, -1, 8, 3, 4, 3, 99], 8, 0)
        self.check2([3, 3, 1107, -1, 8, 3, 4, 3, 99], 12, 0)
        self.check2([3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9], -5, 1)
        self.check2([3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9], 0, 0)
        self.check2([3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9], 7, 1)
        self.check2([3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1], -5, 1)
        self.check2([3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1], 0, 0)
        self.check2([3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1], 7, 1)
        self.check2(
            [3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106, 0, 36, 98, 0, 0, 1002, 21,
             125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99], 7, 999)
        self.check2(
            [3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106, 0, 36, 98, 0, 0, 1002, 21,
             125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99], 8,
            1000)
        self.check2(
            [3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106, 0, 36, 98, 0, 0, 1002, 21,
             125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99], 1008,
            1001)
        # self.check1(intcodes, expected_result)
        # self.check2(intcodes, input, expected_output)

    def check1(self, intcodes, expected_result, input: int = None):
        ic = IntCodeComputer5.simple_evaluate(intcodes, input)
        self.assertEqual(expected_result, getattr(ic, "intcodes"))

    def check2(self, intcodes, input: int, expected_output: int):
        ic = IntCodeComputer5.simple_evaluate(intcodes, input)
        self.assertEqual(expected_output, ic.last_output)
