# --- Part Two ---
# You now have a complete Intcode computer.
#
# Finally, you can lock on to the Ceres distress signal! You just need to boost your sensors using the BOOST program.
#
# The program runs in sensor boost mode by providing the input instruction the value 2. Once run, it will boost the sensors automatically, but it might take a few seconds to complete the operation on slower hardware. In sensor boost mode, the program will output a single value: the coordinates of the distress signal.
#
# Run the BOOST program in sensor boost mode. What are the coordinates of the distress signal?
import itertools
import logging
import os
import sys
import threading
from queue import SimpleQueue
from typing import List

from day7.intcode_computer4 import IntCodeComputer4
from day9.intcode_computer5 import IntCodeComputer5

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s', )

INPUT_FILENAME = "input_intcodes.txt"

def main(argv):
    """
    :param argv: If provided, the first parameter, argv[1] is the path to the input file.
    """

    # Default input path
    dirname = os.path.dirname(__file__)
    inputpath = os.path.join(dirname, INPUT_FILENAME)

    # Check if input path is given as command line argument.
    if len(argv) > 1:
        inputpath = argv[1]

    # Get input file with correct path
    result_msg = "No result"
    try:
        intcodes = []
        logging.debug("Reading input from: " + inputpath)
        with open(inputpath, "r") as f:
            # Read input values as list of ints.
            for line in f:
                linecodes = [int(code) for code in line.split(",")]
                intcodes.extend(linecodes)

        # Run the intcode program.
        ic = IntCodeComputer5.simple_evaluate(intcodes, 2)
        result_msg = "BOOST keycode: " + str(ic.last_output)

    except Exception as e:
        logging.debug(e)

    # Print result
    logging.debug(result_msg)


if __name__ == "__main__":
    main(sys.argv[1:])
