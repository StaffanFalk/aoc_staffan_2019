from unittest import TestCase

from day5.int_code_computer2 import IntCodeComputer2


class TestIntCodeComputer2(TestCase):

    def test_execute_intcodes(self):
        with IntCodeComputer2([1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50]) as icc:
            icc.execute_intcodes()
            self.assertEqual(getattr(icc, "intcodes"), [3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50])

        with IntCodeComputer2([1, 0, 0, 0, 99]) as icc:
            icc.execute_intcodes()
            self.assertEqual(getattr(icc, "intcodes"), [2, 0, 0, 0, 99])

        with IntCodeComputer2([2, 3, 0, 3, 99]) as icc:
            icc.execute_intcodes()
            self.assertEqual(getattr(icc, "intcodes"), [2, 3, 0, 6, 99])

        with IntCodeComputer2([2, 4, 4, 5, 99, 0]) as icc:
            icc.execute_intcodes()
            self.assertEqual(getattr(icc, "intcodes"), [2, 4, 4, 5, 99, 9801])

        with IntCodeComputer2([1, 1, 1, 4, 99, 5, 6, 0, 99]) as icc:
            icc.execute_intcodes()
            self.assertEqual(getattr(icc, "intcodes"), [30, 1, 1, 4, 2, 5, 6, 0, 99])

    def test_extended_functionality(self):
        input = 88
        with IntCodeComputer2([3, 0, 4, 0, 99], input) as icc:
            icc.execute_intcodes()
            self.assertEqual(icc.get_output(), input, "test_extended_functionality 1")

        with IntCodeComputer2([1002, 4, 3, 4, 33]) as icc:
            icc.execute_intcodes()
            self.assertEqual(getattr(icc, "intcodes"), [1002, 4, 3, 4, 99], "test_extended_functionality 2")

        with IntCodeComputer2([1101, 100, -1, 4, 0]) as icc:
            icc.execute_intcodes()
            self.assertEqual(getattr(icc, "intcodes"), [1101, 100, -1, 4, 99], "test_extended_functionality 3")

        with IntCodeComputer2([3, 7, 1, 7, 6, 7, 99, 0]) as icc:
            icc.execute_intcodes()
            self.assertEqual(getattr(icc, "intcodes"), [3, 7, 1, 7, 6, 7, 99, 100], "test_extended_functionality 4")

    def test_puzzle2(self):
        # Test "equal" in positional mode
        with IntCodeComputer2([3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8], 8) as icc:
            icc.execute_intcodes()
            self.assertEqual(icc.get_output(), 1, "test_puzzle2 1")

        with IntCodeComputer2([3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8], 5) as icc:
            icc.execute_intcodes()
            self.assertEqual(icc.get_output(), 0, "test_puzzle2 2")

        with IntCodeComputer2([3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8], 12) as icc:
            icc.execute_intcodes()
            self.assertEqual(icc.get_output(), 0, "test_puzzle2 3")

        # Test "less-than" in positional mode
        with IntCodeComputer2([3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8], 7) as icc:
            icc.execute_intcodes()
            self.assertEqual(icc.get_output(), 1, "test_puzzle2 4")

        # Test "less-than" in positional mode
        with IntCodeComputer2([3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8], 8) as icc:
            icc.execute_intcodes()
            self.assertEqual(icc.get_output(), 0, "test_puzzle2 5")

        # Test "less-than" in positional mode
        with IntCodeComputer2([3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8], 9) as icc:
            icc.execute_intcodes()
            self.assertEqual(icc.get_output(), 0, "test_puzzle2 6")

        # Test "equal" in immediate mode
        with IntCodeComputer2([3, 3, 1108, -1, 8, 3, 4, 3, 99], 6) as icc:
            icc.execute_intcodes()
            self.assertEqual(icc.get_output(), 0, "test_puzzle2 7")

        # Test "equal" in immediate mode
        with IntCodeComputer2([3, 3, 1108, -1, 8, 3, 4, 3, 99], 8) as icc:
            icc.execute_intcodes()
            self.assertEqual(icc.get_output(), 1, "test_puzzle2 8")

        # Test "equal" in immediate mode
        with IntCodeComputer2([3, 3, 1108, -1, 8, 3, 4, 3, 99], 88) as icc:
            icc.execute_intcodes()
            self.assertEqual(icc.get_output(), 0, "test_puzzle2 9")

        # Test "less-than" in immediate mode
        with IntCodeComputer2([3, 3, 1107, -1, 8, 3, 4, 3, 99], 5) as icc:
            icc.execute_intcodes()
            self.assertEqual(icc.get_output(), 1, "test_puzzle2 10")

        # Test "less-than" in immediate mode
        with IntCodeComputer2([3, 3, 1107, -1, 8, 3, 4, 3, 99], 8) as icc:
            icc.execute_intcodes()
            self.assertEqual(icc.get_output(), 0, "test_puzzle2 11")

        # Test "less-than" in immediate mode
        with IntCodeComputer2([3, 3, 1107, -1, 8, 3, 4, 3, 99], 12) as icc:
            icc.execute_intcodes()
            self.assertEqual(icc.get_output(), 0, "test_puzzle2 12")

        # Positional mode
        with IntCodeComputer2([3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9], -5) as icc:
            icc.execute_intcodes()
            self.assertEqual(icc.get_output(), 1, "test_puzzle2 13")

        # Positional mode
        with IntCodeComputer2([3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9], 0) as icc:
            icc.execute_intcodes()
            self.assertEqual(icc.get_output(), 0, "test_puzzle2 14")

        # Positional mode
        with IntCodeComputer2([3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9], 7) as icc:
            icc.execute_intcodes()
            self.assertEqual(icc.get_output(), 1, "test_puzzle2 15")

        # Immediate mode
        with IntCodeComputer2([3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1], -5) as icc:
            icc.execute_intcodes()
            self.assertEqual(icc.get_output(), 1, "test_puzzle2 16")

        # Immediate mode
        with IntCodeComputer2([3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1], 0) as icc:
            icc.execute_intcodes()
            self.assertEqual(icc.get_output(), 0, "test_puzzle2 17")

        # Immediate mode
        with IntCodeComputer2([3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1], 7) as icc:
            icc.execute_intcodes()
            self.assertEqual(icc.get_output(), 1, "test_puzzle2 18")

        # Larger data 1
        with IntCodeComputer2(
                [3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106, 0, 36, 98, 0, 0, 1002, 21,
                 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99],
                7) as icc:
            icc.execute_intcodes()
            self.assertEqual(icc.get_output(), 999, "test_puzzle2 19")

        # Larger data 2
        with IntCodeComputer2(
                [3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106, 0, 36, 98, 0, 0, 1002, 21,
                 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99],
                8) as icc:
            icc.execute_intcodes()
            self.assertEqual(icc.get_output(), 1000, "test_puzzle2 20")

        # Larger data 2
        with IntCodeComputer2(
                [3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106, 0, 36, 98, 0, 0, 1002, 21,
                 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99],
                1008) as icc:
            icc.execute_intcodes()
            self.assertEqual(icc.get_output(), 1001, "test_puzzle2 21")
