import logging
import re
import sys
from typing import List, Dict

import numpy as np

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s', )

class Mapping:
    BLANK = 0
    LEFT = 1
    UP = 2
    RIGHT = 3
    DOWN = 4
    BLACK = 0
    WHITE = 1
    INVALID = -1

    symbol_mapping: Dict[int, str] = {BLACK: ".", WHITE: "#", INVALID: "?"}
    overlay_mapping = {BLANK: " ", LEFT: "<", UP: "^", RIGHT: ">", DOWN: "v"}

    def __init__(self, map_data):
        self.map_data = map_data
        rows = []
        for line in map_data:
            row = [self.inverse_lookup(x, self.symbol_mapping) for x in line.strip()]
            rows.append(row)
        self.height = len(rows)
        self.width = len(map_data[0].strip())
        # print(rows)
        self.map = np.array(rows).reshape(self.height, self.width)
        self.overlay: Dict[str:str] = {}

    def __str__(self):
        lines = ""
        for i, row in enumerate(self.map):
            line_list = []
            for j, x in enumerate(row):
                p = [i, j]

                if str(p) in self.overlay:
                    symbol = self.overlay_mapping[self.overlay[str(p)]]
                else:
                    symbol = self.symbol_mapping[self.map[i, j]]
                line_list.append(symbol)
            line = "".join(line_list)
            lines += (line + "\n")
        return lines

    def inverse_lookup(self, v, mapping):
        try:
            return  next(key for key, value in mapping.items() if value == v)
        except:
            return -1
        # if not p_str is None:
        #     return [ int(x) for x in re.findall(r'\d+', p_str)]
        # return None


    def set_overlay(self, value, p):
        # logging.debug("Set overlay, direction = {}, position = {}".format(value, p))
        self.overlay[str([p[1], p[0]])] = value

    def remove_overlay(self, p:List[int]):
        # logging.debug("Remove overlay, position = {}".format(p))
        del self.overlay[str([p[1], p[0]])]

    def get_value_points(self, value: int):
        return [[p[1], p[0]] for p in np.argwhere(self.map == value).tolist()]

    def check_point_strict_on_line(selfx, p, p1, p2) -> bool:
        x = p[0]
        y = p[1]
        x1 = p1[0]
        y1 = p1[1]
        x2 = p2[0]
        y2 = p2[1]

        return y * (x2 - x1) == x * (y2 - y1) + y1 * (x2 - x1) - x1 * (y2 - y1)

    def get_line(self, p1, p2):
        x1 = p1[0]
        y1 = p1[1]
        x2 = p2[0]
        y2 = p2[1]
        points = []
        issteep = abs(y2 - y1) > abs(x2 - x1)
        if issteep:
            x1, y1 = y1, x1
            x2, y2 = y2, x2
        rev = False
        if x1 > x2:
            x1, x2 = x2, x1
            y1, y2 = y2, y1
            rev = True
        deltax = x2 - x1
        deltay = abs(y2 - y1)
        error = int(deltax / 2)
        y = y1
        ystep = None
        if y1 < y2:
            ystep = 1
        else:
            ystep = -1
        for x in range(x1, x2 + 1):
            if issteep:
                p = [y, x]
                if self.check_point_strict_on_line(p, p1, p2):
                    points.append(p)
            else:
                p = [x, y]
                if self.check_point_strict_on_line(p, p1, p2):
                    points.append(p)
            error -= deltay
            if error < 0:
                y += ystep
                error += deltax
        # Reverse the list if the coordinates were reversed
        if rev:
            points.reverse()
        return points

    def get_value(self, p: List[int]) -> int:
        return self.map[p[1], p[0]]

    def set_value(self, p: List[int], v: int):
        self.map[p[1], p[0]] = v


def main(argv):
    data = ["...........",
            "...........",
            "...........",
            "...........",
            "...........",
            "...........",
            "...........",
            "...........",
            "...........",
            "...........",
            "..........."]

    m = Mapping(data)
    m.set_overlay(3, [3, 3])
    print(m)
    m.remove_overlay([3,3])
    m.set_overlay(2, [3, 3])
    print(m)


if __name__ == "__main__":
    main(sys.argv[1:])
