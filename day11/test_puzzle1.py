import os
from unittest import TestCase

from day11.mapping import Mapping
from day11.painting_robot import PaintingRobot
from util.intcode_utils import read_intcode_file

INPUT_FILENAME = "input_intcodes.txt"


class TestPuzzle1(TestCase):
    def test1(self):
        map_data = [".....",
                    ".....",
                    ".....",
                    ".....",
                    "....."]

        dirname = os.path.dirname(__file__)
        inputpath = os.path.join(dirname, INPUT_FILENAME)
        intcodes = read_intcode_file(inputpath)
        robot = PaintingRobot(Mapping(map_data), [3, 3], intcodes)
        robot.start()
