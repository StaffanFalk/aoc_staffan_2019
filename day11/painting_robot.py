import logging
import traceback
from typing import List, Set

from day11.intcode_computer6 import IntCodeComputer6
from day11.mapping import Mapping

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s', )

class PaintingRobot():
    BLACK = 0
    WHITE = 1
    TURN_LEFT = 0
    TURN_RIGHT = 1
    WAITING_FOR_COLOR = 1
    WAITING_FOR_DIRECTION = 2


    def __init__(self, map: Mapping, start_position: List[int], intcodes: List[int]):
        self.map = map
        self.position = start_position
        self.direction = Mapping.UP
        self.status = self.WAITING_FOR_COLOR
        self.computer = IntCodeComputer6(intcodes, self)
        self.computer.connect_consumer(self)
        self.map.set_overlay(self.direction, self.position)
        self.move_count = 0
        self.painted: Set[str] = set()

    def start(self):
        try:
            self.computer.start()
            self.computer.join()
        except Exception as e:
            traceback.print_exc()

    def get_input(self):
        return self.map.get_value(self.position)

    def put_input(self, signal):
        if self.status == self.WAITING_FOR_COLOR:
            self.paint(signal)
            self.status = self.WAITING_FOR_DIRECTION
        elif self.status == self.WAITING_FOR_DIRECTION:
            if signal == self.TURN_LEFT:
                if self.direction == Mapping.LEFT:
                    self.direction = Mapping.DOWN
                    self.map.remove_overlay(self.position)
                    self.position[1] += 1
                elif self.direction == Mapping.DOWN:
                    self.direction = Mapping.RIGHT
                    self.map.remove_overlay(self.position)
                    self.position[0] += 1
                elif self.direction == Mapping.RIGHT:
                    self.direction = Mapping.UP
                    self.map.remove_overlay(self.position)
                    self.position[1] -= 1
                elif self.direction == Mapping.UP:
                    self.direction = Mapping.LEFT
                    self.map.remove_overlay(self.position)
                    self.position[0] -= 1
            if signal == self.TURN_RIGHT:
                if self.direction == Mapping.LEFT:
                    self.direction = Mapping.UP
                    self.map.remove_overlay(self.position)
                    self.position[1] -= 1
                elif self.direction == Mapping.UP:
                    self.direction = Mapping.RIGHT
                    self.map.remove_overlay(self.position)
                    self.position[0] += 1
                elif self.direction == Mapping.RIGHT:
                    self.direction = Mapping.DOWN
                    self.map.remove_overlay(self.position)
                    self.position[1] += 1
                elif self.direction == Mapping.DOWN:
                    self.direction = Mapping.LEFT
                    self.map.remove_overlay(self.position)
                    self.position[0] -= 1
            self.status = self.WAITING_FOR_COLOR

            self.map.set_overlay(self.direction, self.position)
            # print("{}: New position: {}".format(str(self.move_count), str(self.position)))
            self.move_count += 1
            # print(self.map)

    def paint(self, color):
        # logging.debug("Painting {} with color {}".format(str(self.position), str(color)))
        self.painted.add(str(self.position))
        self.map.set_value(self.position, color)

    def get_paint_count(self):
        return len(self.painted)