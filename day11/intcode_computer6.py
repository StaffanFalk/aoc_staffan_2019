import logging
import threading
import time
import traceback
from queue import SimpleQueue
from typing import List

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s', )


class DummyReceiver:
    def put_input(self, signal: int):
        logging.info("Dummyreceiver got input: " + str(signal))


class IntCodeComputer6(threading.Thread):
    POSITION_MODE = 0
    IMMEDIATE_MODE = 1
    RELATIVE_MODE = 2

    @staticmethod
    def simple_evaluate(intcodes: List[int], input_producer: int = None):
        ic = IntCodeComputer6(intcodes, input_producer)
        ic.connect_consumer(DummyReceiver())
        ic.start()
        ic.join()
        return ic

    def __init__(self, intcodes: List[int], input_producer):
        super(IntCodeComputer6, self).__init__()
        self.intcodes = intcodes[:]
        self.input_queue = SimpleQueue()
        self.output_consumers = []
        self.input_index = 0
        self.last_output = -1
        self.relative_base = 0
        self.intcodes_len = len(intcodes)
        self.extra_memory = {}
        self.input_producer = input_producer

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, tb):
        if exc_type:
            logging.debug(f'type: {exc_type}')
            logging.debug(f'value: {exc_value}')
            traceback.print_exc()

    def run(self):
        self.execute_intcodes()

    def read_memory(self, index):
        if index < 0:
            raise IndexError("Reading negative index: " + str(index))
        if index < self.intcodes_len:
            return self.intcodes[index]
        elif index in self.extra_memory:
            return self.extra_memory[index]
        else:
            return 0

    def write_memory(self, index, value):
        if index < 0:
            raise IndexError("Writing negative index: " + str(index))
        if index < self.intcodes_len:
            self.intcodes[index] = value
        else:
            self.extra_memory[index] = value

    def connect_consumer(self, consumer):
        self.output_consumers.append(consumer)

    def is_connected(self):
        return len(self.output_consumers) > 0

    def get_param_value(self, param, parameter_mode, writing: bool = False):
        if writing:
            if parameter_mode == self.RELATIVE_MODE:
                return self.relative_base + param
            else:
                return param
        else:
            if parameter_mode == self.POSITION_MODE:
                return self.read_memory(param)
            if parameter_mode == self.RELATIVE_MODE:
                pos = self.relative_base + param
                if pos < 0:
                    raise ValueError("get_param_value, Relative parameter-mode give invalid index: " + str(pos))
                return self.read_memory(pos)
            else:
                return param

    def check_params(self, *args):
        for param in args:
            if param < 0 or param >= len(self.intcodes):
                # logging.info("Param " + str(param) + " using extended memory!")
                return True
        return True

    def eval_add(self, ip):
        """
        Evaluate opcode 1
        :param ip: The index of the executing opcode.
        :return: Always True - meaning that execution of the IntCode program should continue.
        """
        INSTRUCTION_LEN = 4
        param0 = self.get_param_value(self.read_memory(ip + 1), self.get_param_mode(ip, 0))
        param1 = self.get_param_value(self.read_memory(ip + 2), self.get_param_mode(ip, 1))
        param2 = self.get_param_value(self.read_memory(ip + 3), self.get_param_mode(ip, 2), True)
        # logging.debug("ADD:        p0={}, p1={}, p2={}".format(param0, param1, param2))
        # logging.debug("Writing value: {}, to index: {}".format(param0 + param1, param2))
        if self.check_params(param2):
            self.write_memory(param2, param0 + param1)
        return ip + INSTRUCTION_LEN

    def eval_multiply(self, ip):
        """
        Evaluate opcode 2
        :param ip: The index of the executing opcode.
        :return: Always True - meaning that execution of the IntCode program should continue.
        """
        INSTRUCTION_LEN = 4
        param0 = self.get_param_value(self.read_memory(ip + 1), self.get_param_mode(ip, 0))
        param1 = self.get_param_value(self.read_memory(ip + 2), self.get_param_mode(ip, 1))
        param2 = self.get_param_value(self.read_memory(ip + 3), self.get_param_mode(ip, 2), True)
        # logging.debug("MULTIPLY:   p0={}, p1={}, p2={}".format(param0, param1, param2))
        # logging.debug("Writing value: {}, to index: {}".format(param0 * param1, param2))
        if self.check_params(param2):
            self.write_memory(param2, param0 * param1)
        return ip + INSTRUCTION_LEN

    def eval_input(self, ip):
        """
        Evaluate opcode 3
        :param ip: The index of the executing opcode.
        :return: Always True - meaning that execution of the IntCode program should continue.
        """
        INSTRUCTION_LEN = 2
        param0 = self.read_memory(ip + 1)
        # logging.debug("INPUT:      p0={}".format(param0))
        signal = self.get_input()
        param0 = self.get_param_value(self.read_memory(ip + 1), self.get_param_mode(ip, 0), True)
        # logging.debug("Writing value: {}, to index: {}".format(signal, param0))
        if self.check_params(param0):
            self.write_memory(param0, signal)
        return ip + INSTRUCTION_LEN

    def eval_output(self, ip):
        """
        Evaluate opcode 4
        :param ip: The index of the executing opcode.
        :return: Always True - meaning that execution of the IntCode program should continue.
        """
        INSTRUCTION_LEN = 2
        param0 = self.get_param_value(self.read_memory(ip + 1), self.get_param_mode(ip, 0))
        # logging.debug("OUTPUT:     p0={}".format(param0))
        while not self.is_connected():
            logging.info("Computer output not connected")
            time.sleep(1)
        self.put_output(param0)
        self.last_output = param0
        return ip + INSTRUCTION_LEN

    def eval_jump_if_true(self, ip):
        """
        Jump-if-true
        :param ip: The index of the executing opcode.
        :return: Always True - meaning that execution of the IntCode program should continue.
        """
        INSTRUCTION_LEN = 3
        param0 = self.get_param_value(self.read_memory(ip + 1), self.get_param_mode(ip, 0))
        param1 = self.get_param_value(self.read_memory(ip + 2), self.get_param_mode(ip, 1))
        # logging.debug("JUMP-TRUE:  p0={}, p1={}".format(param0, param1))
        dojump = param0 != 0
        ip = ip + INSTRUCTION_LEN
        if dojump:
            ip = param1
            # logging.debug("Jumping to index: {}".format(ip))
        return ip

    def eval_jump_if_false(self, ip):
        """
        Jump-if-true
        :param ip: The index of the executing opcode.
        :return: Always True - meaning that execution of the IntCode program should continue.
        """
        INSTRUCTION_LEN = 3
        param0 = self.get_param_value(self.read_memory(ip + 1), self.get_param_mode(ip, 0))
        param1 = self.get_param_value(self.read_memory(ip + 2), self.get_param_mode(ip, 1))
        # logging.debug("JUMP-FALSE: p0={}, p1={}".format(param0, param1))
        dojump = param0 == 0
        ip = ip + INSTRUCTION_LEN
        if dojump:
            ip = param1
            # logging.debug("Jumping to index: {}".format(ip))
        return ip

    def eval_less_than(self, ip):
        """
        less-than
        :param ip: The index of the executing opcode.
        :return: Always True - meaning that execution of the IntCode program should continue.
        """
        INSTRUCTION_LEN = 4
        param0 = self.get_param_value(self.read_memory(ip + 1), self.get_param_mode(ip, 0))
        param1 = self.get_param_value(self.read_memory(ip + 2), self.get_param_mode(ip, 1))
        param2 = self.get_param_value(self.read_memory(ip + 3), self.get_param_mode(ip, 2), True)
        # logging.debug("LESS-THAN:  p0={}, p1={}, p2={}".format(param0, param1, param2))
        if self.check_params(param2):
            result = 0
            if param0 < param1:
                result = 1
            # logging.debug("Writing value: {}, to index: {}".format(result, param2))
            self.write_memory(param2, result)
        return ip + INSTRUCTION_LEN

    def eval_equal(self, ip):
        """
        equals
        :param index: The index of the executing opcode.
        :return: Always True - meaning that execution of the IntCode program should continue.
        """
        INSTRUCTION_LEN = 4
        param0 = self.get_param_value(self.read_memory(ip + 1), self.get_param_mode(ip, 0))
        param1 = self.get_param_value(self.read_memory(ip + 2), self.get_param_mode(ip, 1))
        param2 = self.get_param_value(self.read_memory(ip + 3), self.get_param_mode(ip, 2), True)
        # logging.debug("EQUAL:   :  p0={}, p1={}, p2={}".format(param0, param1, param2))
        if self.check_params(param2):
            result = 0
            if param0 == param1:
                result = 1
            # logging.debug("Writing value: {}, to index: {}".format(result, param2))
            self.write_memory(param2, result)
        return ip + INSTRUCTION_LEN

    def eval_relative_base(self, ip):
        """
        equals
        :param index: The index of the executing opcode.
        :return: Always True - meaning that execution of the IntCode program should continue.
        """
        INSTRUCTION_LEN = 2
        param0 = self.get_param_value(self.read_memory(ip + 1), self.get_param_mode(ip, 0))
        # logging.debug("RELATIVE:   p0={}".format(param0))
        self.relative_base += param0
        # logging.debug("New Relative Base: {}".format(self.relative_base))
        return ip + INSTRUCTION_LEN

    def eval_quit(self, ip):
        """
        Evaluate opcode 99
        :param ip: The index of the executing opcode.
        :return: Always False - meaning that execution of the IntCode program should terminate.
        """
        # logging.debug("QUIT:")
        return -1

    # Dispatcher of the opcodes - easy to extend with new opcodes
    opcode_dispatcher = {
        1: {"eval": eval_add, "name": "add"},
        2: {"eval": eval_multiply, "name": "multiply"},
        3: {"eval": eval_input, "name": "input"},
        4: {"eval": eval_output, "name": "output"},
        5: {"eval": eval_jump_if_true, "name": "jump_if_true"},
        6: {"eval": eval_jump_if_false, "name": "jump_if_false"},
        7: {"eval": eval_less_than, "name": "less_than"},
        8: {"eval": eval_equal, "name": "equal"},
        9: {"eval": eval_relative_base, "name": "relative_base"},
        99: {"eval": eval_quit, "name": "quit"},
    }

    def get_param_mode(self, ip: int, param_index: int) -> int:
        instruction = str(self.read_memory(ip))
        if param_index > len(instruction) - 3:
            return 0
        return int(instruction[-3 - param_index])

    def execute_instruction_code(self, ip):
        instruction_code = self.read_memory(ip)
        il = [int(x) for x in str(instruction_code)]
        opcode = il[-1]
        if len(il) > 1:
            opcode = opcode + il[-2] * 10
        if opcode in self.opcode_dispatcher:
            opcode_evaluator = self.opcode_dispatcher[opcode]
            # logging.debug("Index: " + str(ip) + ", opcode: " + str(opcode) + " (" + opcode_evaluator["name"] + ")")
            return (opcode_evaluator["eval"])(self, ip)
        else:
            raise Exception("Invalid opcode <" + str(opcode) + "> at index: " + str(ip))

    def execute_intcodes(self) -> int:
        """
        Execute the IntCode program.
        :return: The result - the value and index 0.
        """
        try:
            self.i = 0
            while (True):
                # logging.debug("Index: " + str(self.i) + ", instruction: " + str(self.intcodes[self.i]))
                j = self.execute_instruction_code(self.i)
                if j > 0:
                    self.i = j
                else:
                    break
        except Exception as e:
            traceback.print_exc()
        return self.last_output

    def get_input(self):
        signal = self.input_producer.get_input()
        # logging.debug("Got input: " + str(signal))
        return signal

    def put_output(self, signal):
        # logging.debug("Sending output: " + str(signal))
        for consumer in self.output_consumers:
            consumer.put_input(signal)
