from unittest import TestCase

from day10.asteroid_map import AsteroidMap


class TestPuzzle2(TestCase):
    def test1(self):
        asteroid_data = [".#....#####...#..",
                         "##...##.#####..##",
                         "##...#...#.#####.",
                         "..#.....#...###..",
                         "..#.#.....#....##"]

        map = AsteroidMap(asteroid_data)
        m, score = map.find_best_location()
        print("Monitor station: " + str(m))
        map.mark_monitor_station(m)
        print(map)
        print()
        shots = map.shoot_asteroids_clockwise(m)
        self.assertEqual([[8, 1], [9, 0], [9, 1], [10, 0], [9, 2], [11, 1], [12, 1], [11, 2], [15, 1], [12, 2], [13, 2],
                          [14, 2], [15, 2], [12, 3], [13, 3], [14, 3], [16, 4], [15, 4], [10, 4],[4, 4]], shots[0:20])

    def test2(self):
        asteroid_data = [".#..##.###...#######",
                         "##.############..##.",
                         ".#.######.########.#",
                         ".###.#######.####.#.",
                         "#####.##.#.##.###.##",
                         "..#####..#.#########",
                         "####################",
                         "#.####....###.#.#.##",
                         "##.#################",
                         "#####.##.###..####..",
                         "..######..##.#######",
                         "####.##.####...##..#",
                         ".#####..#.######.###",
                         "##...#.##########...",
                         "#.##########.#######",
                         ".####.#.###.###.#.##",
                         "....##.##.###..#####",
                         ".#.#.###########.###",
                         "#.#.#.#####.####.###",
                         "###.##.####.##.#..##"]

        map = AsteroidMap(asteroid_data)
        m, score = map.find_best_location()
        print("Monitor station: " + str(m))
        map.mark_monitor_station(m)
        print(map)
        print()
        shots = map.shoot_asteroids_clockwise(m)
        # for i, s in enumerate(shots):
        #     print("{}: {}".format(i, s))

        self.assertEqual([[11,12], [12,1], [12,2]], shots[0:3])
        self.assertEqual([12,8], shots[9])
        self.assertEqual([16,0], shots[19])
        self.assertEqual([16,9], shots[49])
        self.assertEqual([10,16], shots[99])
        self.assertEqual([9,6], shots[198])
        self.assertEqual([8,2], shots[199])
        self.assertEqual([10,9], shots[200])
        self.assertEqual([11,1], shots[298])
        self.assertEqual([11,1], shots[-1])
