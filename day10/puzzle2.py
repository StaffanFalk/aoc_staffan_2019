# --- Part Two ---
# Once you give them the coordinates, the Elves quickly deploy an Instant Monitoring Station to the location and discover the worst: there are simply too many asteroids.
#
# The only solution is complete vaporization by giant laser.
#
# Fortunately, in addition to an asteroid scanner, the new monitoring station also comes equipped with a giant rotating laser perfect for vaporizing asteroids. The laser starts by pointing up and always rotates clockwise, vaporizing any asteroid it hits.
#
# If multiple asteroids are exactly in line with the station, the laser only has enough power to vaporize one of them before continuing its rotation. In other words, the same asteroids that can be detected can be vaporized, but if vaporizing one asteroid makes another one detectable, the newly-detected asteroid won't be vaporized until the laser has returned to the same position by rotating a full 360 degrees.
#
# For example, consider the following map, where the asteroid with the new monitoring station (and laser) is marked X:
#
# .#....#####...#..
# ##...##.#####..##
# ##...#...#.#####.
# ..#.....X...###..
# ..#.#.....#....##
# The first nine asteroids to get vaporized, in order, would be:
#
# .#....###24...#..
# ##...##.13#67..9#
# ##...#...5.8####.
# ..#.....X...###..
# ..#.#.....#....##
# Note that some asteroids (the ones behind the asteroids marked 1, 5, and 7) won't have a chance to be vaporized until the next full rotation. The laser continues rotating; the next nine to be vaporized are:
#
# .#....###.....#..
# ##...##...#.....#
# ##...#......1234.
# ..#.....X...5##..
# ..#.9.....8....76
# The next nine to be vaporized are then:
#
# .8....###.....#..
# 56...9#...#.....#
# 34...7...........
# ..2.....X....##..
# ..1..............
# Finally, the laser completes its first full rotation (1 through 3), a second rotation (4 through 8), and vaporizes the last asteroid (9) partway through its third rotation:
#
# ......234.....6..
# ......1...5.....7
# .................
# ........X....89..
# .................
# In the large example above (the one with the best monitoring station location at 11,13):
#
# The 1st asteroid to be vaporized is at 11,12.
# The 2nd asteroid to be vaporized is at 12,1.
# The 3rd asteroid to be vaporized is at 12,2.
# The 10th asteroid to be vaporized is at 12,8.
# The 20th asteroid to be vaporized is at 16,0.
# The 50th asteroid to be vaporized is at 16,9.
# The 100th asteroid to be vaporized is at 10,16.
# The 199th asteroid to be vaporized is at 9,6.
# The 200th asteroid to be vaporized is at 8,2.
# The 201st asteroid to be vaporized is at 10,9.
# The 299th and final asteroid to be vaporized is at 11,1.
# The Elves are placing bets on which will be the 200th asteroid to be vaporized. Win the bet by determining which asteroid that will be; what do you get if you multiply its X coordinate by 100 and then add its Y coordinate? (For example, 8,2 becomes 802.)
#
import logging
import os
import sys
import traceback

import numpy as np

from day10.asteroid_map import AsteroidMap

INPUT_FILENAME = "input_asteroid_map.txt"

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s', )

def main(argv):
    """
    :param argv: If provided, the first parameter, argv[1] is the path to the input file.
    """

    # Default input path
    dirname = os.path.dirname(__file__)
    inputpath = os.path.join(dirname, INPUT_FILENAME)

    # Check if input path is given as command line argument.
    if len(argv) > 1:
        inputpath = argv[1]

    # Get input file with correct path
    result_msg = "No result"
    try:
        lines = []
        logging.debug("Reading input from: " + inputpath)
        with open(inputpath, "r") as f:
            # Read input values as list of ints.
            for line in f:
                lines.append(line)
        map = AsteroidMap(lines)
        m, score = map.find_best_location()
        result_msg = "Best location is: " + str(m) + ", reaching " + str(score) + " asteroids."
        shots = map.shoot_asteroids_clockwise(m)
        shot200 = shots[199]
        print(shot200)
        result = shot200[0] * 100 + shot200[1]
        result_msg = "Result: " + str(result)

    except Exception as e:
        traceback.print_exc()

    # Print result
    print(result_msg)


if __name__ == "__main__":
    main(sys.argv[1:])
