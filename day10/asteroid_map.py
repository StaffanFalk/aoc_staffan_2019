import math
import operator
from typing import List

import numpy as np


class AsteroidMap:

    def __init__(self, map_data):
        self.map_data = map_data
        rows = []
        for line in map_data:
            row = [1 if x == "#" else 0 for x in line.strip()]
            rows.append(row)
        self.height = len(rows)
        self.width = len(map_data[0].strip())
        # print(rows)
        self.map = np.array(rows).reshape(self.height, self.width)

    def __str__(self):
        lines = ""
        for row in self.map:
            line = "".join(['#' if x == 1 else 'X' if x == 2 else '.' for x in row])
            lines += (line + "\n")
        return lines

    def get_asteroids(self):
        return [[p[1], p[0]] for p in np.argwhere(self.map == 1).tolist()]

    def check_point(selfx, p, p1, p2) -> bool:
        x = p[0]
        y = p[1]
        x1 = p1[0]
        y1 = p1[1]
        x2 = p2[0]
        y2 = p2[1]

        return y*(x2 - x1) == x*(y2 - y1) + y1*(x2 - x1) - x1*(y2 - y1)

    def get_line(self, p1, p2):
        x1 = p1[0]
        y1 = p1[1]
        x2 = p2[0]
        y2 = p2[1]
        points:List[List[int]] = []
        issteep = abs(y2 - y1) > abs(x2 - x1)
        if issteep:
            x1, y1 = y1, x1
            x2, y2 = y2, x2
        rev = False
        if x1 > x2:
            x1, x2 = x2, x1
            y1, y2 = y2, y1
            rev = True
        deltax = x2 - x1
        deltay = abs(y2 - y1)
        error = int(deltax / 2)
        y = y1
        ystep = None
        if y1 < y2:
            ystep = 1
        else:
            ystep = -1
        for x in range(x1, x2 + 1):
            if issteep:
                p = [y, x]
                if self.check_point(p, p1, p2):
                    points.append(p)
            else:
                p = [x, y]
                if self.check_point(p, p1, p2):
                    points.append(p)
            error -= deltay
            if error < 0:
                y += ystep
                error += deltax
        # Reverse the list if the coordinates were reversed
        if rev:
            points.reverse()
        return points

    def is_asteroid(self, p):
        # print("Point " + str(p) + ": " + str(self.map[p[0], p[1]]))
        if self.map[p[1], p[0]] == 1:
            # print("Result True")
            return True
        # print("Result False")
        return False

    def is_blocked(self, p1, p2):
        '''Check if there is an astoroid on the line between p1 and p2'''
        line = self.get_line(p1, p2)
        # print("Line: " + str(line))
        asteroids = self.get_asteroids()
        # print("asteroids: " + str(asteroids))
        result = False
        for p in line:
            if not p in [p1, p2]:
                if self.is_asteroid(p):
                    result = True
                    break

        # print("is_blocked(" + str(p1) + ", " + str(p2) + ") => " + str(result))
        return result

    def find_best_location(self):
        scores = {}
        asteroids = self.get_asteroids()
        # len_asteroids = len(asteroids)
        for i, m in enumerate(asteroids):
            score = 0
            for a in asteroids:
                if a != m:
                    if not self.is_blocked(m, a):
                        score += 1
            scores[i] = score
            # print(score)
            # print()
        # print(asteroids)
        # print(scores)
        best_i = max(scores.items(), key=operator.itemgetter(1))[0]
        return asteroids[best_i], scores[best_i]

    def get_asteroids_info(self, monitor_station):
        asteroids = self.get_asteroids()
        asteroids_info = []
        for asteroid in asteroids:
            asteroid_info = {}
            asteroid_info["coord"] = asteroid
            asteroid_info["angle"] = self.calc_angle(monitor_station, asteroid)
            asteroids_info.append(asteroid_info)
        return sorted(asteroids_info, key=lambda i: i['angle'])


    def shoot_asteroids_clockwise(self, monitor):
        i = 1
        j = 1
        shots = []
        last_angle = -1
        while True:
            last_angle = -1
            # print("Iteration " + str(i) + ". j = " + str(j))
            asteroids_info = self.get_asteroids_info(monitor)
            if len(asteroids_info) == 0:
                break
            for asteroid_info in asteroids_info:
                if asteroid_info["angle"] != last_angle:
                    if not self.is_blocked(monitor, asteroid_info["coord"]):
                        # print(str(j) + ": Shooting asteroid at " + str(asteroid_info["coord"]) + ", at angle: " +
                        #                                                str((asteroid_info["angle"] * 180) / math.pi))
                        # if j in [100]:
                        #     print(self)
                        shots.append(self.shoot(asteroid_info["coord"]))
                        last_angle = asteroid_info["angle"]
                        j += 1
            i += 1
        return shots

    def shoot(self, target):
        self.map[target[1], target[0]] = 0
        return target

    def mark_monitor_station(self, asteroid):
        self.map[asteroid[1], asteroid[0]] = 2

    def find_first_target(self, monitor):
        for y in range(monitor[1] + 1, self.height):
            if self.is_asteroid(monitor[0], y):
                return [monitor[0], y]

    @staticmethod
    def determine_quadrant(p):
        if p[0] >= 0 and p[1] >= 0:
            return 1
        if p[0] >= 0 and p[1] < 0:
            return 2
        if p[0] < 0 and p[1] < 0:
            return 3
        if p[0] < 0 and p[1] >= 0:
            return 4

    @staticmethod
    def calc_angle(monitor_station, asteroid):
        rel_coord = [asteroid[0] - monitor_station[0], -(asteroid[1] - monitor_station[1])]
        # print("rel_coord: " + str(rel_coord))
        q = AsteroidMap.determine_quadrant(rel_coord)
        # print("quadrant: " + str(q))
        if rel_coord[1] == 0:
            if q < 3:
                return math.pi / 2
            return 3 * math.pi / 2
        angle = math.atan(rel_coord[0]/rel_coord[1])
        if q == 1:
            return angle
        if q == 2 or q == 3:
            return math.pi + angle
        return 2 * math.pi + angle
#
#
# m = [0,0]
# s = [-1,-10]
# a = AsteroidMap.calc_angle(m, s)
# print("Radians: " + str(a))
# print("Degrees: " + str(a * 180 / math.pi))