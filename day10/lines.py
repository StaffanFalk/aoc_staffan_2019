from numpy import *
def perp( a ) :
    b = empty_like(a)
    b[0] = -a[1]
    b[1] = a[0]
    return b

# line segment a given by endpoints a1, a2
# line segment b given by endpoints b1, b2
# return
def seg_intersect(a1,a2, b1,b2) :
    da = a2-a1
    db = b2-b1
    dp = a1-b1
    dap = perp(da)
    denom = dot( dap, db)
    num = dot( dap, dp )
    return (num / denom.astype(float))*db + b1

p1 = array( [0.0, 0.0] )
p2 = array( [1.0, 0.0] )

p3 = array( [4.0, -5.0] )
p4 = array( [4.0, 2.0] )

print(seg_intersect( p1,p2, p3,p4))

p1 = array( [2.0, 2.0] )
p2 = array( [4.0, 3.0] )

p3 = array( [6.0, 0.0] )
p4 = array( [6.0, 3.0] )

print(seg_intersect( p1,p2, p3,p4))

p5 = array( [1.0, 1.0] )
p6 = array( [6.0, 4.0] )
# p5 = array( [1.0, 1.0] )

print(seg_intersect( p6,p6, p3,p4))
##0123456789
#0......B
#1
#2..A
#3....A.B
#4......A
#5
#6