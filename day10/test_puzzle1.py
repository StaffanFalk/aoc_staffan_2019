from unittest import TestCase

from day10.asteroid_map import AsteroidMap


class TestPuzzle1(TestCase):
    def test1(self):
        asteroid_data = [".#..#",
                         ".....",
                         "#####",
                         "....#",
                         "...##"]
        map = AsteroidMap(asteroid_data)
        print(map)
        print()
        m, score = map.find_best_location()
        self.assertEqual([3, 4], m)
        self.assertEqual(score, 8)

    def test2(self):
        asteroid_data = ["......#.#.",
                         "#..#.#....",
                         "..#######.",
                         ".#.#.###..",
                         ".#..#.....",
                         "..#....#.#",
                         "#..#....#.",
                         ".##.#..###",
                         "##...#..#.",
                         ".#....####"]
        map = AsteroidMap(asteroid_data)
        print(map)
        print()
        m, score = map.find_best_location()
        self.assertEqual([5, 8], m)
        self.assertEqual(score, 33)

    def test3(self):
        asteroid_data = ["#.#...#.#.",
                         ".###....#.",
                         ".#....#...",
                         "##.#.#.#.#",
                         "....#.#.#.",
                         ".##..###.#",
                         "..#...##..",
                         "..##....##",
                         "......#...",
                         ".####.###."]

        map = AsteroidMap(asteroid_data)
        print(map)
        print()
        m, score = map.find_best_location()
        self.assertEqual([1, 2], m)
        self.assertEqual(score, 35)

    def test4(self):
        asteroid_data = [".#..#..###",
                         "####.###.#",
                         "....###.#.",
                         "..###.##.#",
                         "##.##.#.#.",
                         "....###..#",
                         "..#.#..#.#",
                         "#..#.#.###",
                         ".##...##.#",
                         ".....#.#.."]

        map = AsteroidMap(asteroid_data)
        print(map)
        print()
        m, score = map.find_best_location()
        self.assertEqual([6, 3], m)
        self.assertEqual(score, 41)

    def test5(self):
        asteroid_data = [".#..##.###...#######",
                         "##.############..##.",
                         ".#.######.########.#",
                         ".###.#######.####.#.",
                         "#####.##.#.##.###.##",
                         "..#####..#.#########",
                         "####################",
                         "#.####....###.#.#.##",
                         "##.#################",
                         "#####.##.###..####..",
                         "..######..##.#######",
                         "####.##.####...##..#",
                         ".#####..#.######.###",
                         "##...#.##########...",
                         "#.##########.#######",
                         ".####.#.###.###.#.##",
                         "....##.##.###..#####",
                         ".#.#.###########.###",
                         "#.#.#.#####.####.###",
                         "###.##.####.##.#..##"]

        map = AsteroidMap(asteroid_data)
        print(map)
        print()
        m, score = map.find_best_location()
        self.assertEqual([11, 13], m)
        self.assertEqual(score, 210)
