import itertools
import logging
import os
import sys
import threading
from queue import SimpleQueue
from typing import List

from day7.intcode_computer4 import IntCodeComputer4

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s', )

INPUT_FILENAME = "input_intcodes.txt"

class AmplificationCircuit:
    def __init__(self, intcodes: List[int], phase_sequence: List[int], names: str = None):
        self.intcodes: List[int] = intcodes[:]
        self.phase_sequence: List[int] = phase_sequence[:]
        self.names = names
        self.input_queue = SimpleQueue()
        self.amps = []
        self.output = 0

    def calculate_output_signal(self) -> int:
        for i, phase in enumerate(self.phase_sequence):
            if not self.names is None:
                names = self.names
            else:
                names = range(1, len(self.phase_sequence) + 1)
            amp = Amp(names[i], self.intcodes, phase)
            self.amps.append(amp)
            if i > 0:
                self.amps[i-1].connect_consumer(self.amps[i])
        self.amps[-1].connect_consumer(self.amps[0])
        self.amps[-1].connect_consumer(self)
        self.amps[0].send_signal(0)
        self.start()
        self.join()
        return self.output

    def start(self):
        for amp in self.amps:
            amp.start()

    def join(self):
        for amp in self.amps:
            amp.join()

    def send_signal(self, signal):
        # logging.info("Output: " + str(signal))
        self.output = signal

class Amp(threading.Thread):
    def __init__(self, name: str, intcodes: List[int], phase: int):
        super(Amp, self).__init__()
        self.name = name
        self.intcodes = intcodes[:]
        self.phase = phase
        self.output_signal = 0
        self.computer = IntCodeComputer4(self.intcodes)
        self.send_signal(phase)

    def connect_receiver(self, receiver):
        self.computer.connect_receiver(receiver)

    def send_signal(self, signal):
        self.computer.input_queue.put(signal)

    def calculate_output_signal(self):
        logging.debug("Amp " + self.name + " started")
        signal = self.computer.execute_intcodes()
        logging.debug("Amp(name={}, phase={}) => {}".format(self.name, self.phase, signal))
        return signal

    def run(self):
        self.calculate_output_signal()

    # Sequence:
    # 1. Get current phase: input                -> output: phase (0-4)
    # 2. Get input_signal:  input (input-signal) -> output: output signal
    # 3. Output instruction
    #
    # instruction 0 = phase settings
    # instruction 1 = input signal -> output - signal


def main(argv):
    """
    :param argv: If provided, the first parameter, argv[1] is the path to the input file.
    """

    # Default input path
    dirname = os.path.dirname(__file__)
    inputpath = os.path.join(dirname, INPUT_FILENAME)

    # Check if input path is given as command line argument.
    if len(argv) > 1:
        inputpath = argv[1]

    # Get input file with correct path
    result_msg = "No result"
    try:
        intcodes = []
        with open(inputpath, "r") as f:
            # Read input values as list of ints.
            for line in f:
                linecodes = [int(code) for code in line.split(",")]
                intcodes.extend(linecodes)

        # Run the intcode program.
        phases_input = [5, 6, 7, 8, 9]
        phases_perm = itertools.permutations(phases_input)
        max_output = 0
        for phases in list(phases_perm):
            # logging.debug(phases)
            output = AmplificationCircuit(intcodes, phases, "ABCDE").calculate_output_signal()
            max_output = max(output, max_output)
            logging.info("Phases " + str(phases) + " => " + str(output))
        result_msg = "Max output: " + str(max_output)

    except Exception as e:
        logging.debug(e)

    # Print result
    logging.debug(result_msg)


if __name__ == "__main__":
    main(sys.argv[1:])
