import itertools
import os
import sys
from typing import List

from day7.intcode_computer3 import IntCodeComputer3
from day7.intcode_computer4 import IntCodeComputer4

INPUT_FILENAME = "input_intcodes.txt"

class AmplificationCircuit:
    def __init__(self, intcodes: List[int], phase_sequence: List[int], names: List[str] = None):
        self.intcodes: List[int] = intcodes
        self.phase_sequence: List[int] = phase_sequence
        self.names = names
        self.output_signal: int = 0

    def calculate_output_signal(self) -> int:
        signal: int = 0
        for i, phase in enumerate(self.phase_sequence):
            if not self.names is None:
                names = self.names
            else:
                names = range(1, len(self.phase_sequence) + 1)
            signal = Amp(names[i], self.intcodes, phase, signal).calculate_output_signal()
        self.output_signal = signal
        return self.output_signal

class Amp:
    def __init__(self, name: str, intcodes: List[int], phase: int, input_signal: int = 0):
        self.name = name
        self.intcodes = intcodes[:]
        self.phase = phase
        self.input_signal = input_signal
        self.output_signal = 0
        self.computer = IntCodeComputer3(self.intcodes, [self.phase, self.input_signal])

    def set_input_signal(self, input_signal):
        self.input_signal = input_signal

    def calculate_output_signal(self):
        self.output_signal = self.computer.execute_intcodes()
        print("Amp(name={}, phase={}, input={}) => {}".format(self.name, self.phase, self.input_signal, self.output_signal))
        return self.output_signal


    # Sequence:
    # 1. Get current phase: input                -> output: phase (0-4)
    # 2. Get input_signal:  input (input-signal) -> output: output signal
    # 3. Output instruction
    #
    # instruction 0 = phase settings
    # instruction 1 = input signal -> output - signal


def main(argv):
    """
    :param argv: If provided, the first parameter, argv[1] is the path to the input file.
    """

    # Default input path
    dirname = os.path.dirname(__file__)
    inputpath = os.path.join(dirname, INPUT_FILENAME)

    # Check if input path is given as command line argument.
    if len(argv) > 1:
        inputpath = argv[1]

    # Get input file with correct path
    result_msg = "No result"
    try:
        intcodes = []
        with open(inputpath, "r") as f:
            # Read input values as list of ints.
            for line in f:
                linecodes = [int(code) for code in line.split(",")]
                intcodes.extend(linecodes)

        # Run the intcode program.
        phases_input = [0, 1, 2, 3, 4]
        phases_perm = itertools.permutations(phases_input)
        max_output = 0
        for phases in list(phases_perm):
            print(phases)
            output = AmplificationCircuit(intcodes, phases, "ABCDE").calculate_output_signal()
            max_output = max(output, max_output)

        result_msg = "Max output: " + str(max_output)

    except Exception as e:
        print(e)

    # Print result
    print(result_msg)


if __name__ == "__main__":
    main(sys.argv[1:])
