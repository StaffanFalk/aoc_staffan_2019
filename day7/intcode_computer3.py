import traceback
from typing import List

class IntCodeComputer3:

    def __init__(self, intcodes: List[int], input: List[int]):
        self.intcodes = intcodes[:]
        self.input = input
        self.input_index = 0
        self.output = ""

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, tb):
        if exc_type:
            print(f'type: {exc_type}')
            print(f'value: {exc_value}')
            traceback.print_exc()

    POSITION_MODE = 0
    IMMEDIATE_MODE = 1

    def get_param_value(self, param, parameter_mode):
        if parameter_mode == self.POSITION_MODE:
            return self.intcodes[param]
        else:
            return param

    def check_params(self, *args):
        for param in args:
            if param < 0 or param >= len(self.intcodes):
                print("Warning: Param " + str(param) + " outside limits!")
                return False
        return True

    def eval_add(self, ip):
        """
        Evaluate opcode 1
        :param ip: The index of the executing opcode.
        :return: Always True - meaning that execution of the IntCode program should continue.
        """
        INSTRUCTION_LEN = 4
        param0 = self.get_param_value(self.intcodes[ip + 1], self.get_param_mode(ip, 0))
        param1 = self.get_param_value(self.intcodes[ip + 2], self.get_param_mode(ip, 1))
        param2 = self.intcodes[ip + 3]
        if self.check_params(param2):
            self.intcodes[param2] = param0 + param1
        return ip + INSTRUCTION_LEN

    def eval_multiply(self, ip):
        """
        Evaluate opcode 2
        :param ip: The index of the executing opcode.
        :return: Always True - meaning that execution of the IntCode program should continue.
        """
        INSTRUCTION_LEN = 4
        param0 = self.get_param_value(self.intcodes[ip + 1], self.get_param_mode(ip, 0))
        param1 = self.get_param_value(self.intcodes[ip + 2], self.get_param_mode(ip, 1))
        param2 = self.intcodes[ip + 3]
        if self.check_params(param2):
            self.intcodes[param2] = param0 * param1
        return ip + INSTRUCTION_LEN

    def eval_input(self, ip):
        """
        Evaluate opcode 3
        :param ip: The index of the executing opcode.
        :return: Always True - meaning that execution of the IntCode program should continue.
        """
        INSTRUCTION_LEN = 2
        param0 = self.intcodes[ip + 1]
        if self.check_params(param0):
            self.intcodes[param0] = self.get_next_input()
        return ip + INSTRUCTION_LEN

    def eval_output(self, ip):
        """
        Evaluate opcode 4
        :param ip: The index of the executing opcode.
        :return: Always True - meaning that execution of the IntCode program should continue.
        """
        INSTRUCTION_LEN = 2
        param0 = self.get_param_value(self.intcodes[ip + 1], self.get_param_mode(ip, 0))
        self.output = self.output + str(param0)
        return ip + INSTRUCTION_LEN

    def eval_jump_if_true(self, ip):
        """
        Jump-if-true
        :param ip: The index of the executing opcode.
        :return: Always True - meaning that execution of the IntCode program should continue.
        """
        INSTRUCTION_LEN = 3
        param0 = self.get_param_value(self.intcodes[ip + 1], self.get_param_mode(ip, 0))
        param1 = self.get_param_value(self.intcodes[ip + 2], self.get_param_mode(ip, 1))
        dojump = param0 != 0
        ip = ip + INSTRUCTION_LEN
        if dojump:
            ip = param1
        return ip

    def eval_jump_if_false(self, ip):
        """
        Jump-if-true
        :param ip: The index of the executing opcode.
        :return: Always True - meaning that execution of the IntCode program should continue.
        """
        INSTRUCTION_LEN = 3
        param0 = self.get_param_value(self.intcodes[ip + 1], self.get_param_mode(ip, 0))
        param1 = self.get_param_value(self.intcodes[ip + 2], self.get_param_mode(ip, 1))
        dojump = param0 == 0
        ip = ip + INSTRUCTION_LEN
        if dojump:
            ip = param1
        return ip


    def eval_less_than(self, ip):
        """
        less-than
        :param ip: The index of the executing opcode.
        :return: Always True - meaning that execution of the IntCode program should continue.
        """
        INSTRUCTION_LEN = 4
        param0 = self.get_param_value(self.intcodes[ip + 1], self.get_param_mode(ip, 0))
        param1 = self.get_param_value(self.intcodes[ip + 2], self.get_param_mode(ip, 1))
        param2 = self.intcodes[ip + 3]
        if self.check_params(param2):
            result = 0
            if param0 < param1:
                result = 1
            self.intcodes[param2] = result
        return ip + INSTRUCTION_LEN

    def eval_equal(self, ip):
        """
        equals
        :param index: The index of the executing opcode.
        :return: Always True - meaning that execution of the IntCode program should continue.
        """
        INSTRUCTION_LEN = 4
        param0 = self.get_param_value(self.intcodes[ip + 1], self.get_param_mode(ip, 0))
        param1 = self.get_param_value(self.intcodes[ip + 2], self.get_param_mode(ip, 1))
        param2 = self.intcodes[ip + 3]
        if self.check_params(param2):
            result = 0
            if param0 == param1:
                result = 1
            self.intcodes[param2] = result
        return ip + INSTRUCTION_LEN

    def eval_quit(self, ip):
        """
        Evaluate opcode 99
        :param ip: The index of the executing opcode.
        :return: Always False - meaning that execution of the IntCode program should terminate.
        """
        return -1

    # Dispatcher of the opcodes - easy to extend with new opcodes
    opcode_dispatcher = {
        1: { "eval": eval_add},
        2: { "eval": eval_multiply},
        3: { "eval": eval_input},
        4: { "eval": eval_output},
        5: { "eval": eval_jump_if_true},
        6: { "eval": eval_jump_if_false},
        7: { "eval": eval_less_than},
        8: { "eval": eval_equal},
        99: { "eval": eval_quit},
    }

    def get_param_mode(self, ip: int, param_index: int) -> int:
        instruction = str(self.intcodes[ip])
        if  param_index > len(instruction) -3:
            return 0
        return int(instruction[-3 - param_index])

    def execute_instruction_code(self, ip):
        instruction_code = self.intcodes[ip]
        il = [int(x) for x in str(instruction_code)]
        opcode = il[-1]
        if len(il) > 1:
            opcode = opcode + il[-2] * 10
        if opcode in self.opcode_dispatcher:
            opcode_evaluator = self.opcode_dispatcher[opcode]
            return (opcode_evaluator["eval"])(self, ip)
        else:
            raise Exception("Invalid opcode <" + str(opcode) + "> at index: " + str(ip))

    def execute_intcodes(self) -> int:
        """
        Execute the IntCode program.
        :return: The result - the value and index 0.
        """
        self.i = 0
        while(True):
            j = self.execute_instruction_code(self.i)
            if j > 0:
                self.i = j
            else:
                break
        return self.get_output()

    def get_output(self):
        return int(self.output)

    def get_next_input(self):
        if self.input_index < len(self.input):
            input = self.input[self.input_index]
            self.input_index += 1
        else:
            raise ValueError("Too many input instructions: " + self.input_index + 1)
        return input