import copy
from typing import List


class FuelFactory:
    def __init__(self):
        self.rules = {
            # product_name: (product_count, dict(resource_name: resource_count)
        }

        self.required = {
            # resource_name: resource_count
        }

        self.store = {
            # resource_name: resource_count
        }

    def parse(self, lines: List[str]):
        for line in lines:
            self.parseRule(line)

    def parseRule(self, line):
        line_parts = line.split("=>")
        left_parts = line_parts[0].strip().split(",")
        right_parts = line_parts[1].strip().split(" ")
        product_count = int(right_parts[0].strip())
        product = right_parts[1].strip()
        resources = {}
        for item in left_parts:
            resource_parts = item.strip().split((" "))
            resource_count = int(resource_parts[0].strip())
            resource_name = resource_parts[1].strip()
            resources[resource_name] = resource_count
        self.rules[product] = (product_count, resources)

    def calc_needed_resources(self, product_needed_amount: int, product_name: str) -> int:
        # for each needed resource - put the resource into store
        #
        # Repeat until all done
        #   for each resource in store
        #       break it down into components
        #       store each component
        #       remove resource
        #
        self.put_resource(self.required, product_name, product_needed_amount)
        while not self.break_down_resources():
            pass

        # print(self.store)
        result = 0
        if "ORE" in self.required:
            return self.required["ORE"]

    def break_down_resources(self):
        # print(self.store)
        done = True
        required_copy = copy.deepcopy(self.required)
        for product_name in self.required:
            if product_name in self.rules:
                done = False
                multi = 1
                needed_product_count = self.required[product_name]
                if self.has_resource(self.store, product_name, needed_product_count):
                    self.get_resource(self.store, product_name, needed_product_count)
                    self.get_resource(required_copy, product_name, needed_product_count)
                else:
                    produced_product_count = self.rules[product_name][0]
                    if produced_product_count < needed_product_count:
                        multi = needed_product_count // produced_product_count
                        if multi * produced_product_count < needed_product_count:
                            multi += 1
                    required_product_count = multi * produced_product_count
                    if required_product_count > needed_product_count:
                        self.put_resource(self.store, product_name, required_product_count - needed_product_count)
                    self.get_resource(required_copy, product_name, needed_product_count)
                    # self.get_resource(store_copy, product_name, required_product_count - needed_product_count)
                    resources_dict = self.rules[product_name][1]
                    for resource_name in resources_dict:
                        self.put_resource(required_copy, resource_name, multi * resources_dict[resource_name])
        required_copy_copy = copy.deepcopy(required_copy)
        for product_name in required_copy:
            if product_name in self.store:
                done = False
                store_count = self.store[product_name]
                required_count = required_copy[product_name]
                if required_count >= store_count:
                    self.get_resource(required_copy_copy, product_name, store_count)
                    self.get_resource(self.store, product_name, store_count)
                else:
                    use_count = store_count - required_count
                    self.get_resource(required_copy_copy, product_name, use_count)
                    self.get_resource(self.store, product_name, use_count)

        if not done:
            self.required = required_copy_copy
        return done

    def put_resource(self, store, product_name, amount):
        if product_name in store:
            store[product_name] += amount
        else:
            store[product_name] = amount

    def get_resource(self, store, product_name, amount):
        store[product_name] -= amount
        if store[product_name] == 0:
            del store[product_name]
        return amount

    def has_resource(self, store, product_name, amount):
        if product_name in store:
            if store[product_name] >= amount:
                return True
        return False



