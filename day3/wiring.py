from typing import List, Tuple, Set


# Represents an electric wire on a grid
class Wire:
    def __init__(self, start_coord: Tuple[int, int], wire_directions: str):
        self.start_coord = start_coord
        self.coords = self.get_wire_coordinates(wire_directions)

    def get_wire_coordinates(self, wire_directions) -> List[Tuple[int, int]]:
        last_coord = self.start_coord
        coords = [last_coord]
        dir_list = wire_directions.split(",")
        for direction in dir_list:
            x = last_coord[0]
            y = last_coord[1]
            d = int(direction[1:])
            if d != 0:
                if direction[0] == "R":
                    for i in range(0, d):
                        x += 1
                        coords.append((x, y))
                elif direction[0] == "D":
                    for i in range(0, d):
                        y -= 1
                        coords.append((x, y))
                elif direction[0] == "L":
                    for i in range(0, d):
                        x -= 1
                        coords.append((x, y))
                elif direction[0] == "U":
                    for i in range(0, d):
                        y += 1
                        coords.append((x, y))
                last_coord = (x, y)
        return coords

    def get_latency(self, point_coord):
        """
        Calculate the "latency" in the wire to the specified point.
        :param point_coord: The point to which the latency is calculated.
        :return:    The "latency"
        """
        try:
            return self.coords.index(point_coord)
        except ValueError:
            return -1


# Helper class for electric wiring
class Wiring:

    @staticmethod
    def find_intersections(wire1: Wire, wire2: Wire, skip_first: bool = True) -> Set[Tuple[int, int]]:
        if skip_first:
            coords1 = set(wire1.coords[1:])
            coords2 = set(wire2.coords[1:])
        else:
            coords1 = set(wire1.coords)
            coords2 = set(wire2.coords)

        return coords1.intersection(coords2)

    @staticmethod
    def get_distance(coord1: Tuple[int, int], coord2: Tuple[int, int]):
        return abs(coord2[0] - coord1[0]) + abs(coord2[1] - coord1[1])

    @staticmethod
    def get_distances(coords: Set[Tuple[int, int]], ref_coord: Tuple[int, int]) -> List[int]:
        return [Wiring.get_distance(c, ref_coord) for c in coords]

    @staticmethod
    def find_closest_intersection(start_coord, wire1: Wire, wire2: Wire):
        intersections = Wiring.find_intersections(wire1, wire2)
        return min(Wiring.get_distances(intersections, start_coord))

    @staticmethod
    def find_shortest_combined_latency(wire1: Wire, wire2: Wire):
        intersections = Wiring.find_intersections(wire1, wire2)
        latencies = [wire1.get_latency(i) + wire2.get_latency(i) for i in intersections]
        return min(latencies)
