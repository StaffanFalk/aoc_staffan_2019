from unittest import TestCase

from day3.wiring import Wire, Wiring


class TestWiring(TestCase):
    def check_shortest_distance(self, d1: str, d2: str, expected_distance: int):
        w1 = Wire((0, 0), d1)
        w2 = Wire((0, 0), d2)
        distance = Wiring.find_closest_intersection((0, 0), w1, w2)
        self.assertEqual(expected_distance, distance)

    def check_shortest_latency(self, d1: str, d2: str, expected_latency: int):
        w1 = Wire((0, 0), d1)
        w2 = Wire((0, 0), d2)
        latency = Wiring.find_shortest_combined_latency(w1, w2)
        self.assertEqual(expected_latency, latency)

    def test_find_closest_intersection1(self):
        self.check_shortest_distance("R8,U5,L5,D3", "U7,R6,D4,L4", 6)

    def test_find_closest_intersection2(self):
        self.check_shortest_distance("R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83", 159)

    def test_find_closest_intersection3(self):
        self.check_shortest_distance("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51",
                                     "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7", 135)

    def test_find_shortest_combined_latency1(self):
        self.check_shortest_latency("R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83", 610)

    def test_find_shortest_combined_latency2(self):
        self.check_shortest_latency("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51",
                                    "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7", 410)
