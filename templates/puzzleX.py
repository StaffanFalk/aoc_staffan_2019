import logging
import os
import sys
import traceback

INPUT_FILENAME = "input_file.txt"

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s', )

def main(argv):
    """
    :param argv: If provided, the first parameter, argv[1] is the path to the input file.
    """

    # Default input path
    dirname = os.path.dirname(__file__)
    inputpath = os.path.join(dirname, INPUT_FILENAME)

    # Check if input path is given as command line argument.
    if len(argv) > 1:
        inputpath = argv[1]

    # Get input file with correct path
    result_msg = "No result"
    try:
        print(inputpath)

    except Exception as e:
        traceback.print_exc()

    # Print result
    print(result_msg)


if __name__ == "__main__":
    main(sys.argv[1:])
