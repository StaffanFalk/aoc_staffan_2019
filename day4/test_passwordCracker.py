from unittest import TestCase

from day4.puzzle1 import PasswordCracker


class TestPasswordCracker(TestCase):
    def test_validate(self):
        pc = PasswordCracker("123257-647015".split("-"))
        self.assertFalse(pc.validate("123113"))
        self.assertFalse(pc.validate("124423"))
        self.assertFalse(pc.validate("123257"))
        self.assertFalse(pc.validate("647015"))
        self.assertFalse(pc.validate("123456"))
        self.assertFalse(pc.validate("123387"))
        self.assertTrue(pc.validate("123357"))
