from unittest import TestCase

from day4.puzzle2 import PasswordCracker


class TestPasswordCracker2(TestCase):
    def test_validate(self):
        pc = PasswordCracker("123257-647015".split("-"))
        self.assertFalse(pc.validate2("123113"))
        self.assertFalse(pc.validate2("124423"))
        self.assertFalse(pc.validate2("123257"))
        self.assertFalse(pc.validate2("647015"))
        self.assertFalse(pc.validate2("123456"))
        self.assertFalse(pc.validate2("123387"))
        self.assertTrue(pc.validate2("123357"))
        self.assertFalse(pc.validate2("123337"))
        self.assertTrue(pc.validate2("133388"))
        self.assertTrue(pc.validate2("223338"))
        self.assertTrue(pc.validate2("222338"))
        self.assertFalse(pc.validate2("222238"))
        self.assertTrue(pc.validate2("222277"))
        self.assertFalse(pc.validate2("222267"))
        self.assertFalse(pc.validate2("222227"))
        self.assertFalse(pc.validate2("222222"))
