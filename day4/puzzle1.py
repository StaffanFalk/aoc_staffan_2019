# --- Day 4: Secure Container ---
# You arrive at the Venus fuel depot only to discover it's protected by a password. The Elves had written the password on a sticky note, but someone threw it out.
#
# However, they do remember a few key facts about the password:
#
# It is a six-digit number.
# The value is within the range given in your puzzle input.
# Two adjacent digits are the same (like 22 in 122345).
# Going from left to right, the digits never decrease; they only ever increase or stay the same (like 111123 or 135679).
# Other than the range rule, the following are true:
#
# 111111 meets these criteria (double 11, never decreases).
# 223450 does not meet these criteria (decreasing pair of digits 50).
# 123789 does not meet these criteria (no double).
# How many different passwords within the range given in your puzzle input meet these criteria?

import sys
import traceback

from typing import List


class PasswordCracker:
    def __init__(self, password_range: List[str]):
        self.password_range = password_range
        self.min = int(password_range[0])
        self.max = int(password_range[1])

    def crack(self) -> int:
        result = 0
        for i in range(self.min, self.max):
            if self.validate(str(i)):
                result += 1

        return result

    def crack(self) -> int:
        result = 0
        for i in range(self.min, self.max):
            if self.validate(str(i)):
                result += 1

        return result

    def validate(self, s: str):
        if s < self.password_range[0] and s > self.password_range[1]:
            return False
        for i in range(1, len(s)):
            if s[i] < s[i-1]:
                return False
        for i in range(1, len(s)):
            if s[i] == s[i-1]:
                return True
        return False

    def validate2(self, s: str):
        if s < self.password_range[0] and s > self.password_range[1]:
            return False
        for i in range(1, len(s)):
            if s[i] < s[i-1]:
                return False
        in_pair = False
        for i in range(1, len(s)):
            if s[i] == s[i-1]:
                if not in_pair:
                    in_pair = True
                else:
                    in_pair = False
            else:
                if in_pair:
                    return True
        return False



def main(argv):
    password_range = "123257-647015".split("-")

    # Get input file with correct path
    result_msg = "No result"
    try:
        count = PasswordCracker(password_range).crack()
        result_msg = "Password count: " + str(count)

    except Exception as e:
        traceback.print_exc()
        print(e)

    # Print result
    print(result_msg)


if __name__ == "__main__":
    main(sys.argv[1:])
