from unittest import TestCase

from day2.int_code_computer import IntCodeComputer


class TestIntCodeComputer(TestCase):

    def test_execute_intcodes(self):
        with IntCodeComputer([1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50]) as icc:
            icc.execute_intcodes()
            self.assertEqual(getattr(icc, "intcodes"), [3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50])

        with IntCodeComputer([1, 0, 0, 0, 99]) as icc:
            icc.execute_intcodes()
            self.assertEqual(getattr(icc, "intcodes"), [2, 0, 0, 0, 99])

        with IntCodeComputer([2, 3, 0, 3, 99]) as icc:
            icc.execute_intcodes()
            self.assertEqual(getattr(icc, "intcodes"), [2, 3, 0, 6, 99])

        with IntCodeComputer([2, 4, 4, 5, 99, 0]) as icc:
            icc.execute_intcodes()
            self.assertEqual(getattr(icc, "intcodes"), [2, 4, 4, 5, 99, 9801])

        with IntCodeComputer([1, 1, 1, 4, 99, 5, 6, 0, 99]) as icc:
            icc.execute_intcodes()
            self.assertEqual(getattr(icc, "intcodes"), [30, 1, 1, 4, 2, 5, 6, 0, 99])
