from typing import List


class IntCodeComputer:

    def __init__(self, intcodes: List[int]):
        self.intcodes = intcodes

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type:
            print(f'type: {exc_type}')
            print(f'value: {exc_value}')
            print(f'traceback: {traceback}')

    def eval_opcode_1(self, index):
        """
        Evaluate opcode 1
        :param index: The index of the executing opcode.
        :return: Always True - meaning that execution of the IntCode program should continue.
        """
        self.intcodes[self.intcodes[index + 3]] = \
            self.intcodes[self.intcodes[index + 1]] + self.intcodes[self.intcodes[index + 2]]
        return True

    def eval_opcode_2(self, index):
        """
        Evaluate opcode 2
        :param index: The index of the executing opcode.
        :return: Always True - meaning that execution of the IntCode program should continue.
        """
        self.intcodes[self.intcodes[index + 3]] = \
            self.intcodes[self.intcodes[index + 1]] * self.intcodes[self.intcodes[index + 2]]
        return True

    def eval_opcode_99(self, index):
        """
        Evaluate opcode 99
        :param index: The index of the executing opcode.
        :return: Always False - meaning that execution of the IntCode program should terminate.
        """
        return False

    # Dispatcher of the opcodes - easy to extend with new opcodes
    opcode_dispatcher = {
        1: eval_opcode_1,
        2: eval_opcode_2,
        99: eval_opcode_99,
    }

    def eval_opcode(self, opcode_index: int) -> bool:
        """
        Evaluate an opcode.
        :param opcode_index:
        :return:
        """
        opcode = self.intcodes[opcode_index]
        if opcode in self.opcode_dispatcher:
            return (IntCodeComputer.opcode_dispatcher[opcode])(self, opcode_index)
        else:
            raise Exception("Invalid opcode <" + str(opcode) + "> at index: " + str(opcode_index))

    def execute_intcodes(self) -> int:
        """
        Execute the IntCode program.
        :return: The result - the value and index 0.
        """
        for i in range(0, len(self.intcodes), 4):
            if not self.eval_opcode(i):
                break

        return self.intcodes[0]
