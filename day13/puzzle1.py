# --- Day 13: Care Package ---
# As you ponder the solitude of space and the ever-increasing three-hour roundtrip for messages between you and Earth, you notice that the Space Mail Indicator Light is blinking. To help keep you sane, the Elves have sent you a care package.
#
# It's a new game for the ship's arcade cabinet! Unfortunately, the arcade is all the way on the other end of the ship. Surely, it won't be hard to build your own - the care package even comes with schematics.
#
# The arcade cabinet runs Intcode software like the game the Elves sent (your puzzle input). It has a primitive screen capable of drawing square tiles on a grid. The software draws tiles to the screen with output instructions: every three output instructions specify the x position (distance from the left), y position (distance from the top), and tile id. The tile id is interpreted as follows:
#
# 0 is an empty tile. No game object appears in this tile.
# 1 is a wall tile. Walls are indestructible barriers.
# 2 is a block tile. Blocks can be broken by the ball.
# 3 is a horizontal paddle tile. The paddle is indestructible.
# 4 is a ball tile. The ball moves diagonally and bounces off objects.
# For example, a sequence of output values like 1,2,3,6,5,4 would draw a horizontal paddle tile (1 tile from the left and 2 tiles from the top) and a ball tile (6 tiles from the left and 5 tiles from the top).
#
# Start the game. How many block tiles are on the screen when the game exits?
#
#
import logging
import os
import sys
import traceback

from day13.intcode_computer7 import IntCodeComputer7
from util.intcode_utils import read_intcode_file

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s', )

INPUT_FILENAME = "input_intcodes.txt"

EMPTY_TILE = 0
WALL_TILE = 1
BLOCK_TILE = 2
PADDLE_TILE = 3
BALL_TILE = 4

def main(argv):
    """
    :param argv: If provided, the first parameter, argv[1] is the path to the input file.
    """

    # Default input path
    dirname = os.path.dirname(__file__)
    inputpath = os.path.join(dirname, INPUT_FILENAME)

    # Check if input path is given as command line argument.
    if len(argv) > 1:
        inputpath = argv[1]

    # Get input file with correct path
    result_msg = "No result"
    try:
        logging.debug("Reading input from: " + inputpath)
        intcodes = read_intcode_file(inputpath)
        ic = IntCodeComputer7.simple_evaluate(intcodes)
        output = ic.output_consumers[0].received
        block_commands = [output[x:x + 3] for x in range(0, len(output), 3) if output[x + 2] == BLOCK_TILE]
        result_msg = "Block commands: " + str(len(block_commands))

    except Exception as e:
        traceback.print_exc()
        logging.debug(e)

    # Print result
    logging.debug(result_msg)


if __name__ == "__main__":
    main(sys.argv[1:])
