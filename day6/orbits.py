from typing import List, Tuple, Optional, Iterable, Dict


class Orbits(object):
    def __init__(self, orbits: Dict[str, str]):
        self.orbits = orbits

    def count_orbits(self) -> int:
        total_count = 0
        for key in self.orbits:
            total_count += self.count_orbit_trail(key, None, [key])[0]
        return total_count

    def count_orbit_trail(self, begin: str, end: str = None, trail: List[str] = None) -> Tuple[
        int, Optional[List[str]]]:
        skip_first = False
        if trail is None or trail == []:
            skip_first = True
        count, trail = self.__count_orbit_trail(begin, end, trail)
        if skip_first:
            count -= 1
        return count, trail

    def __count_orbit_trail(self, current: str, end: str = None, trail: List[str] = None) -> Tuple[
        int, Optional[List[str]]]:
        if (end is None or current != end) and current in self.orbits:
            next_key = self.orbits[current]
            if not trail is None:
                trail.append(next_key)
            count, trail = self.count_orbit_trail(next_key, end, trail)
            count += 1
            return count, trail
        return 0, trail

    def get_trail(self, begin: str, end: str = None) -> Tuple[int, Optional[List[str]]]:
        trail = [begin]
        return self.count_orbit_trail(begin, end, trail)

    def count_moves(self, start: str, end: str):
        start_count, start_trail = self.count_orbit_trail(start, None, [])
        end_count, end_trail = self.count_orbit_trail(end, None, [])
        common_object = None
        for object in start_trail:
            if object in end_trail:
                common_object = object
                break
        # print(common_object)
        if not common_object is None:
            start_count, start_trail = self.count_orbit_trail(start, common_object, [])
            end_count, end_trail = self.count_orbit_trail(end, common_object, [])
            # print(start_trail)
            # print(end_trail)
            end_trail.reverse()
            # print(end_trail)
            # print("start: " + str(start_count))
            # print("end: " + str(end_count))
            result = start_count + end_count
            result_trail = start_trail[:-1] + end_trail
        else:
            result = -1
            result_trail = []
        return result, result_trail

    @staticmethod
    def add_orbit_from_symbols(orbit_symbols: Iterable[str], orbits: Dict[str, str] = None):
        if orbits is None:
            orbits = {}
        for symbol in orbit_symbols:
            orbit_pair = symbol.split(")")
            orbits[orbit_pair[1].strip()] = orbit_pair[0].strip()
        return orbits
