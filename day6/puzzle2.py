import os
import sys
from typing import Dict, List, Iterable, Tuple, Optional

from day6.orbits import Orbits

INPUT_FILENAME = "input_orbits.txt"



def main(argv):
    """
    :param argv: If provided, the first parameter, argv[1] is the path to the input file.
    """

    # Default input path
    dirname = os.path.dirname(__file__)
    inputpath = os.path.join(dirname, INPUT_FILENAME)

    # Check if input path is given as command line argument.
    if len(argv) > 1:
        inputpath = argv[1]

    # Get input file with correct path
    result_msg = "No result"
    try:
        with open(inputpath, "r") as f:
            # Read input values as list of ints.

            orbits = Orbits(Orbits.add_orbit_from_symbols(f))
            result, trail = orbits.count_moves("YOU", "SAN")
            result_msg = "Orbit count: " + str(result)
    except Exception as e:
        print(e)

    # Print result
    print(result_msg)


if __name__ == "__main__":
    main(sys.argv[1:])
