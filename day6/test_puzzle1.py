from unittest import TestCase

from day6.puzzle1 import Orbits


class TestOrbits(TestCase):
    def test_count_orbits(self):
        orbits = {}
        Orbits.add_orbit_from_symbols(["A)B"], orbits)
        self.assertEqual(1, len(orbits))
        self.assertEqual(1, Orbits(orbits).count_orbits())

        Orbits.add_orbit_from_symbols(["A)B", "B)C"], orbits)
        self.assertEqual(2, len(orbits))
        self.assertEqual(3, Orbits(orbits).count_orbits())

        Orbits.add_orbit_from_symbols(["A)B", "B)C", "A)D"], orbits)
        self.assertEqual(3, len(orbits))
        self.assertEqual(4, Orbits(orbits).count_orbits())

        Orbits.add_orbit_from_symbols(["COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H", "D)I", "E)J", "J)K", "K)L"],
                                      orbits)
        self.assertEqual(11, len(orbits))
        self.assertEqual(42, Orbits(orbits).count_orbits())

        orbits = Orbits(Orbits.add_orbit_from_symbols(["A)B", "B)C"]))
        count, trail = orbits.count_orbit_trail("C", "A", ["C"])
        self.assertEqual(2, count)
        self.assertEqual(trail, ['C', 'B', 'A'])

        count, trail = orbits.count_orbit_trail("C", "B", ["C"])
        self.assertEqual(1, count)
        self.assertEqual(trail, ['C', 'B'])

        # orbits = Orbits(Orbits.add_orbit_from_symbols(["COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H", "D)I", "E)J", "J)K", "K)L", "K)YOU", "I)SAN"]))
        # you_count, you_trail = orbits.count_orbit_trail("YOU", None, [])
        # san_count, san_trail = orbits.count_orbit_trail("SAN", None, [])
        # common_object = None
        # for object in you_trail:
        #     if object in san_trail:
        #         common_object = object
        #         break
        # print(common_object)
        # if not common_object is None:
        #     you_count, you_trail = orbits.count_orbit_trail("YOU", common_object, [])
        #     san_count, san_trail = orbits.count_orbit_trail("SAN", common_object, [])
        #     # print("You")
        #     # print(you_count)
        #     # print(you_trail)
        #     # print("Santa:")
        #     # print(san_count)
        #     # print(san_trail)
        #     self.assertEqual(4, you_count + san_count)
        # else:
        #     self.fail("No common ground")

        orbits = Orbits(Orbits.add_orbit_from_symbols(["COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H", "D)I", "E)J", "J)K", "K)L", "K)YOU", "I)SAN"]))
        count, trail = orbits.count_moves("YOU", "SAN")
        self.assertEqual(4, count)
        print(trail)
