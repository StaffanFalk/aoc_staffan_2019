def read_intcode_file(intcode_filepath: str):
    intcodes = []
    with open(intcode_filepath, "r") as f:
        # Read input values as list of ints.
        for line in f:
            linecodes = [int(code) for code in line.split(",")]
            intcodes.extend(linecodes)
    return intcodes