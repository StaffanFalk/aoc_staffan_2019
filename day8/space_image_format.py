from typing import Collection


class SpaceImageFormat:

    def __init__(self, image_data: Collection, x_dim: int, y_dim: int):
        self.image = None
        self.image_data: Collection = image_data[:]
        self.x_dim: int = x_dim
        self.y_dim: int = y_dim
        layer_size: int = x_dim * y_dim
        self.layers_count: int = len(image_data) // layer_size
        if self.layers_count * layer_size != len(image_data):
            raise ValueError("Corrupt image data")
        self.layers = []

        for l in range(0, self.layers_count):
            self.layers.append(image_data[l * layer_size: (l + 1) * layer_size])

    def get_layers(self):
        return self.layers

    def merge_layers(self):
        z = zip(*self.layers)
        self.image = [self.merge_pixel(p) for p in zip(*self.layers)]
        return self.image

    def get_image(self):
        if self.image == None:
            self.merge_layers()
        return self.image

    INVALID = "9"
    BLACK = "0"
    WHITE = "1"
    TRANSP = "2"

    def print_layer(self, layer, show_char = WHITE):
        for line_index in range(0, self.y_dim):
            print("|" + "".join(['X' if x == show_char else "." for x in layer[line_index * self.x_dim:  (line_index + 1) * self.x_dim]]) + "|")

    def print_image(self):
        self.print_layer(self.get_image())

    def get_pixel(self, x, y):
        if self.pixel_valid(x, y):
            return self.get_image()[x + y * self.x_dim]
        return self.INVALID

    def merge_pixel(self, item):
        for layer in item:
            if layer != self.TRANSP:
                return layer
        return self.TRANSP

    def pixel_valid(self, x, y):
        return 0 <= x < self.x_dim and 0 <= y < self.y_dim
