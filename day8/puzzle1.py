import logging
import os
import sys
from typing import Collection

from day8.space_image_format import SpaceImageFormat

INPUT_FILENAME = "input_image_data.txt"

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s', )

def main(argv):
    """
    :param argv: If provided, the first parameter, argv[1] is the path to the input file.
    """

    # Default input path
    dirname = os.path.dirname(__file__)
    inputpath = os.path.join(dirname, INPUT_FILENAME)

    # Check if input path is given as command line argument.
    if len(argv) > 1:
        inputpath = argv[1]

    # Get input file with correct path
    result_msg = "No result"
    try:
        image_data = []
        with open(inputpath, "r") as f:
            # Read input values as list of ints.
            for line in f:
                image_data.extend(line.strip())
        image = SpaceImageFormat(image_data, 25, 6)
        layers = image.get_layers()
        min_count = sys.maxsize
        min_layer_index = -1
        for i, layer in enumerate(layers):
            count = layer.count("0")
            if count < min_count:
                min_count = count
                min_layer_index = i
                logging.debug("index " + str(i) + ", digit " + str(0) + ", count: " + str(count))

        count1 = layers[min_layer_index].count("1")
        count2 = layers[min_layer_index].count("2")
        logging.debug("index " + str(min_layer_index) + ", digit " + str(1) + ", count: " + str(count1))
        logging.debug("index " + str(min_layer_index) + ", digit " + str(2) + ", count: " + str(count2))
        result = count1 * count2
        result_msg = "Result: " + str(result)

    except Exception as e:
        print(e)

    # Print result
    print(result_msg)


if __name__ == "__main__":
    main(sys.argv[1:])
