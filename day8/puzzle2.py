import logging
import os
import sys
import traceback
from typing import Collection

from day8.space_image_format import SpaceImageFormat

INPUT_FILENAME = "input_image_data.txt"

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s', )

def main(argv):
    """
    :param argv: If provided, the first parameter, argv[1] is the path to the input file.
    """

    # Default input path
    dirname = os.path.dirname(__file__)
    inputpath = os.path.join(dirname, INPUT_FILENAME)

    # Check if input path is given as command line argument.
    if len(argv) > 1:
        inputpath = argv[1]

    # Get input file with correct path
    result_msg = "No result"
    try:
        image_data = []
        with open(inputpath, "r") as f:
            # Read input values as list of ints.
            for line in f:
                image_data.extend(line.strip())

        image = SpaceImageFormat(image_data, 25, 6)
        image.print_image()

    except Exception as e:
        traceback.print_exc()
        # print(e)

if __name__ == "__main__":
    main(sys.argv[1:])
