from unittest import TestCase

from day8.puzzle1 import SpaceImageFormat


class TestSpaceImageFormat(TestCase):
    def test_puzzle1(self):
        layers = SpaceImageFormat("123456789012", 3, 2).get_layers()
        self.assertEqual("123456", layers[0])
        self.assertEqual("789012", layers[1])

    def test_puzzle2(self):
        image = SpaceImageFormat("0222112222120000", 2, 2)
        self.assertEqual(image.BLACK, image.get_pixel(0, 0)) # top-left
        self.assertEqual(image.WHITE, image.get_pixel(1, 0)) # top-right
        self.assertEqual(image.WHITE, image.get_pixel(0, 1)) # bottom-left
        self.assertEqual(image.BLACK, image.get_pixel(1, 1)) # bottom-right
