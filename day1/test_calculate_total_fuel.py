from unittest import TestCase

from day1.puzzle2 import calculate_total_fuel


class TestCalculateTotalTuel(TestCase):
    def test_calculate_total_fuel(self):
        self.assertEqual(calculate_total_fuel([]), 0)
        self.assertEqual(calculate_total_fuel([-12, -6767]), 0)
        self.assertEqual(calculate_total_fuel([14]), 2)
        self.assertEqual(calculate_total_fuel([1969]), 966)
        self.assertEqual(calculate_total_fuel([100756]), 50346)
