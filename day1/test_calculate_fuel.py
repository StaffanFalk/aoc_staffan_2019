from unittest import TestCase

from day1.puzzle2 import calculate_fuel


class TestCalculateFuel(TestCase):
    def test_calculate_fuel(self):
        # Tests from puzzle description
        self.assertEqual(calculate_fuel(12), 2)
        self.assertEqual(calculate_fuel(14), 2)
        self.assertEqual(calculate_fuel(1969), 654)
        self.assertEqual(calculate_fuel(100756), 33583)

        self.assertEqual(calculate_fuel(0), 0)
        self.assertEqual(calculate_fuel(-564), 0)
