from unittest import TestCase

from day1.puzzle1 import calculate_fuel_for_modules


class TestCalculateFuelForModules(TestCase):
    def test_calculate_fuel_for_modules(self):
        self.assertEqual(calculate_fuel_for_modules([]), 0)

        # Tests from puzzle description
        self.assertEqual(calculate_fuel_for_modules([12]), 2)
        self.assertEqual(calculate_fuel_for_modules([14]), 2)
        self.assertEqual(calculate_fuel_for_modules([1969]), 654)
        self.assertEqual(calculate_fuel_for_modules([100756]), 33583)
        self.assertEqual(calculate_fuel_for_modules([12, 14, 1969, 100756]), sum([2, 2, 654, 33583]))
