# --- Part Two --- During the second Go / No Go poll, the Elf in charge of the Rocket Equation Double-Checker stops
# the launch sequence. Apparently, you forgot to include additional fuel for the fuel you just added.
#
# Fuel itself requires fuel just like a module - take its mass, divide by three, round down, and subtract 2. However,
# that fuel also requires fuel, and that fuel requires fuel, and so on. Any mass that would require negative fuel
# should instead be treated as if it requires zero fuel; the remaining mass, if any, is instead handled by wishing
# really hard, which has no mass and is outside the scope of this calculation.
#
# So, for each module mass, calculate its fuel and add it to the total. Then, treat the fuel amount you just
# calculated as the input mass and repeat the process, continuing until a fuel requirement is zero or negative. For
# example:
#
# A module of mass 14 requires 2 fuel. This fuel requires no further fuel (2 divided by 3 and rounded down is 0,
# which would call for a negative fuel), so the total fuel required is still just 2. At first, a module of mass 1969
# requires 654 fuel. Then, this fuel requires 216 more fuel (654 / 3 - 2). 216 then requires 70 more fuel,
# which requires 21 fuel, which requires 5 fuel, which requires no further fuel. So, the total fuel required for a
# module of mass 1969 is 654 + 216 + 70 + 21 + 5 = 966. The fuel required by a module of mass 100756 and its fuel is:
# 33583 + 11192 + 3728 + 1240 + 411 + 135 + 43 + 12 + 2 = 50346. What is the sum of the fuel requirements for all of
# the modules on your spacecraft when also taking into account the mass of the added fuel? (Calculate the fuel
# requirements for each module separately, then add them all up at the end.)
#
import math
import os
import sys
from typing import List


def calculate_fuel(mass: int) -> int:
    """
    Calculate the fuel requirements for items
    :param mass: The mass of an items
    :return: The fuel requirement for the provided item
    """
    return max(math.floor(mass / 3) - 2, 0)


def calculate_total_fuel(module_masses: List[int]) -> int:
    """
    Calculate the total fuel requirements including fuel for fuel
    :param module_masses: List of the masses of the individual modules
    :return: The total fuel requirements
    """
    # Calculate sum for modules
    total_fuel = 0
    for mass in module_masses:
        fuel = calculate_fuel(mass)
        module_fuel = fuel

        while fuel > 0:
            fuel = calculate_fuel(fuel)
            module_fuel += fuel
        total_fuel += module_fuel
    return total_fuel


def main(argv):
    """
    :param argv: If provided, the first parameter, argv[1] is the path to the input file.
    """

    # Default input path
    dirname = os.path.dirname(__file__)
    inputpath = os.path.join(dirname, "fuel_requirements.txt")

    # Check if input path is given as command line argument.
    if len(argv) > 1:
        inputpath = argv[1]

    # Get inputfile with correct path
    result_msg = "No result"
    try:
        with open(inputpath, "r") as f:
            # Read input values as list of ints.
            module_masses = [int(s) for s in f]

        total_fuel = calculate_total_fuel(module_masses)

        # Create result message if success
        result_msg = "Total fuel requirements: " + str(total_fuel)

    except Exception as e:
        print(e)

    # Print result
    print(result_msg)


if __name__ == "__main__":
    main(sys.argv[1:])
