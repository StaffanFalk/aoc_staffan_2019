from unittest import TestCase

import numpy as np

from day12.moon_system import MoonSystem


class TestDay12(TestCase):
    def test1(self):
        moons_def = ["<x=14, y=9, z=14>",
                     "<x=9, y=11, z=6>",
                     "<x=-6, y=14, z=-4>",
                     "<x=4, y=-4, z=-3>"]
        ms = MoonSystem.parse(moons_def)
        self.assertTrue(np.array_equal(ms.positions, [[14, 9, 14], [9, 11, 6], [-6, 14, -4], [4, -4, -3]]))

    def test2(self):
        moons_def = ["<x=-1, y=0, z=2>",
                     "<x=2, y=-10, z=-7>",
                     "<x=4, y=-8, z=8>",
                     "<x=3, y=5, z=-1>"]
        ms = MoonSystem.parse(moons_def)
        for i in range(1, 11):
            ms.time_steps()
            ms.print("After {} time steps:".format(i))
        # pos = < x = 2, y = 1, z = -3 >, vel = < x = -3, y = -2, z = 1 >
        # pos = < x = 1, y = -8, z = 0 >, vel = < x = -1, y = 1, z = 3 >
        # pos = < x = 3, y = -6, z = 1 >, vel = < x = 3, y = 2, z = -3 >
        # pos = < x = 2, y = 0, z = 4 >, vel = < x = 1, y = -1, z = -1 >
        self.assertTrue(np.array_equal(ms.positions, [[2, 1, -3], [1, -8, 0], [3, -6, 1], [2, 0, 4]]))
        self.assertTrue(np.array_equal(ms.velocities, [[-3, -2, 1], [-1, 1, 3], [3, 2, -3], [1, -1, -1]]))

    def test3(self):
        moons_def = ["<x=-8, y=-10, z=0>",
                     "<x=5, y=5, z=10>",
                     "<x=2, y=-7, z=3>",
                     "<x=9, y=-8, z=-3>"]
        ms = MoonSystem.parse(moons_def)
        ms.time_steps(100)
        ms.print("After {} time steps:".format(100))
        # pos = < x = 8, y = -12, z = -9 >, vel = < x = -7, y = 3, z = 0 >
        # pos = < x = 13, y = 16, z = -3 >, vel = < x = 3, y = -11, z = -5 >
        # pos = < x = -29, y = -11, z = -1 >, vel = < x = -3, y = 7, z = 4 >
        # pos = < x = 16, y = -13, z = 23 >, vel = < x = 7, y = 1, z = 1 >
        self.assertTrue(np.array_equal(ms.positions, [[8, -12, -9], [13, 16, -3], [-29, -11, -1], [16, -13, 23]]))
        self.assertTrue(np.array_equal(ms.velocities, [[-7, 3, 0], [3, -11, -5], [-3, 7, 4], [7, 1, 1]]))

        # Energy after 100 steps:
        # pot:  8 + 12 +  9 = 29;   kin: 7 +  3 + 0 = 10;   total: 29 * 10 = 290
        # pot: 13 + 16 +  3 = 32;   kin: 3 + 11 + 5 = 19;   total: 32 * 19 = 608
        # pot: 29 + 11 +  1 = 41;   kin: 3 +  7 + 4 = 14;   total: 41 * 14 = 574
        # pot: 16 + 13 + 23 = 52;   kin: 7 +  1 + 1 =  9;   total: 52 *  9 = 468
        # Sum of total energy: 290 + 608 + 574 + 468 = 1940
        total_energy = ms.calc_total_energy()
        self.assertEqual(1940, total_energy)

    def test2(self):
        moons_def = ["<x=-1, y=0, z=2>",
                     "<x=2, y=-10, z=-7>",
                     "<x=4, y=-8, z=8>",
                     "<x=3, y=5, z=-1>"]
        ms = MoonSystem.parse(moons_def)
        # ms.print("0: hash: " + str(ms.hash()))
        # ms.time_step(2770)
        # ms.print("2770: hash: " + str(ms.hash()))
        # ms.time_step()
        # ms.print("2771: hash: " + str(ms.hash()))
        # ms.time_step()
        # ms.print("2772: hash: " + str(ms.hash()))
        first, second = ms.check_repeated_states()
        ms.print("{}: hash: {}".format(first, second))
        # print("First={}, Second={}".format(format, second))
