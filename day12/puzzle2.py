# --- Part Two ---
# All this drifting around in space makes you wonder about the nature of the universe. Does history really repeat itself? You're curious whether the moons will ever return to a previous state.
#
# Determine the number of steps that must occur before all of the moons' positions and velocities exactly match a previous point in time.
#
# For example, the first example above takes 2772 steps before they exactly match a previous point in time; it eventually returns to the initial state:
#
# After 0 steps:
# pos=<x= -1, y=  0, z=  2>, vel=<x=  0, y=  0, z=  0>
# pos=<x=  2, y=-10, z= -7>, vel=<x=  0, y=  0, z=  0>
# pos=<x=  4, y= -8, z=  8>, vel=<x=  0, y=  0, z=  0>
# pos=<x=  3, y=  5, z= -1>, vel=<x=  0, y=  0, z=  0>
#
# After 2770 steps:
# pos=<x=  2, y= -1, z=  1>, vel=<x= -3, y=  2, z=  2>
# pos=<x=  3, y= -7, z= -4>, vel=<x=  2, y= -5, z= -6>
# pos=<x=  1, y= -7, z=  5>, vel=<x=  0, y= -3, z=  6>
# pos=<x=  2, y=  2, z=  0>, vel=<x=  1, y=  6, z= -2>
#
# After 2771 steps:
# pos=<x= -1, y=  0, z=  2>, vel=<x= -3, y=  1, z=  1>
# pos=<x=  2, y=-10, z= -7>, vel=<x= -1, y= -3, z= -3>
# pos=<x=  4, y= -8, z=  8>, vel=<x=  3, y= -1, z=  3>
# pos=<x=  3, y=  5, z= -1>, vel=<x=  1, y=  3, z= -1>
#
# After 2772 steps:
# pos=<x= -1, y=  0, z=  2>, vel=<x=  0, y=  0, z=  0>
# pos=<x=  2, y=-10, z= -7>, vel=<x=  0, y=  0, z=  0>
# pos=<x=  4, y= -8, z=  8>, vel=<x=  0, y=  0, z=  0>
# pos=<x=  3, y=  5, z= -1>, vel=<x=  0, y=  0, z=  0>
# Of course, the universe might last for a very long time before repeating. Here's a copy of the second example from above:
#
# <x=-8, y=-10, z=0>
# <x=5, y=5, z=10>
# <x=2, y=-7, z=3>
# <x=9, y=-8, z=-3>
# This set of initial positions takes 4686774924 steps before it repeats a previous state! Clearly, you might need to find a more efficient way to simulate the universe.
#
# How many steps does it take to reach the first state that exactly matches a previous state?
#
import logging
import os
import sys
import traceback
from collections import Set

from day12.moon_system import MoonSystem

INPUT_FILENAME = "input_file.txt"

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s', )


def main(argv):
    """
    :param argv: If provided, the first parameter, argv[1] is the path to the input file.
    """

    # Default input path
    dirname = os.path.dirname(__file__)
    inputpath = os.path.join(dirname, INPUT_FILENAME)

    # Check if input path is given as command line argument.
    if len(argv) > 1:
        inputpath = argv[1]

    # Get input file with correct path
    result_msg = "No result"
    try:
        lines = []
        with open(inputpath, "r") as f:
            # Read input values as list of ints.
            for line in f:
                lines.append(line)

        ms = MoonSystem.parse(lines)
        first, second = ms.check_repeated_states()
        ms.print()
        result_msg = "The state after step {0:1d} is repeated after step {1:1d}.".format(first, second)

    except Exception as e:
        traceback.print_exc()

    # Print result
    print(result_msg)


if __name__ == "__main__":
    main(sys.argv[1:])
