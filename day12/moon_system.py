import copy
import re
from itertools import combinations
from typing import List

import numpy as np

class MoonSystem:
    @classmethod
    def parse(cls, lines):
        moons = []
        for line in lines:
            moon = list(map(int, re.findall(r'[-+]?\d+', line)))
            # print(type(moon[0]))
            # print(moon)
            moons.append(moon)
        ms = MoonSystem(moons)
        return ms

    def __init__(self, positions: List[List[int]]):
        # print(rows)
        self.positions = np.array(positions, dtype = int).reshape(len(positions), len(positions[0]))
        # print(self.positions)
        self.velocities = np.zeros([len(positions), len(positions[0])], dtype=int)
        self.backup_positions = copy.deepcopy(self.positions)
        self.backup_velocities = copy.deepcopy(self.velocities)
        # print(self.velocities)

    def print(self, message = ""):
        if len(message) > 0:
            print(message)
        for i, pos in enumerate(self.positions):
            vel = self.velocities[i]
            print("pos=<x={0:2d}, y={1:2d}, z={2:2d}>, vel=<x={3:2d}, y={4:2d}, z={5:2d}>".format(pos[0], pos[1], pos[2], vel[0], vel[1], vel[2]))

    def time_steps(self, steps:int = 1):
        for step in range(0, steps):
            self._apply_gravity()
            self._apply_velocity()

    def time_step(self):
        self._apply_gravity()
        self._apply_velocity()

    def _apply_gravity(self):
        combs = combinations(range(0, len(self.positions)), 2)
        axis_list = list(range(0, 3))
        combs_list = list(combs)
        for comb in combs_list:
            for axis in axis_list:
                # print("{0:1d}, {1:1d}, {2:1d}".format(i, j, axis))
                self._apply_gravity_moon(comb[0], comb[1], axis)

    def _apply_gravity_moon(self, moon1_i: int, moon2_i: int, axis: int):
        if self.positions[moon1_i][axis] < self.positions[moon2_i][axis]:
            self.velocities[moon1_i][axis] += 1
            self.velocities[moon2_i][axis] -= 1
        elif self.positions[moon1_i][axis] > self.positions[moon2_i][axis]:
            self.velocities[moon1_i][axis] -= 1
            self.velocities[moon2_i][axis] += 1

    def _apply_velocity(self):
        for i, moon_pos in enumerate(self.positions):
            moon_vel = self.velocities[i]
            for axis in range(0, 3):
                moon_pos[axis] += moon_vel[axis]

    def calc_total_energy(self):
        total_energy = 0
        for i in range(0, len(self.positions)):
            total_energy += self.calc_total_energy_moon(i)
        return total_energy

    def calc_total_energy_moon(self, i):
        p = self.calc_pot_energy_moon(i)
        k = self.calc_kin_energy_moon(i)
        return p * k

    def calc_pot_energy_moon(self, i):
        pos = self.positions[i]
        return sum([abs(p) for p in pos])

    def calc_kin_energy_moon(self, i):
        vel = self.velocities[i]
        return sum([abs(v) for v in vel])

    def hash(self) -> int:
        pos_hash = hash(self.positions.tobytes())
        vel_hash = hash(self.velocities.tobytes())
        return pos_hash + 10 * vel_hash

    def check_repeated_states(self):
        i = 0
        hashes = {}
        while True:
            self.time_step()
            # h = self.hash()
            # if h in hashes:
            if np.array_equal(self.positions, self.backup_positions):
                if np.array_equal(self.velocities, self.backup_velocities):
                    first = 0
                    # first = hashes[h]
                    second = i
                    break
            else:
                # hashes[h] = i
                if i % 100000 == 0:
                    print(i)
            i += 1
        return first, second
