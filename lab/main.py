import logging
import sys

from lab.consumer import Consumer
from lab.producer import Producer

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s', )


def main(argv):
    try:
        c = Consumer(5)
        p = Producer(5)
        p.connect(c)
        c.start()
        p.start()
        p.join()
        c.join()

    except Exception as e:
        logging.debug("Main: " + e)

    # Print result
    logging.debug("Main Done")


if __name__ == "__main__":
    main(sys.argv[1:])
