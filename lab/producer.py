import logging
import threading
import time

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s', )

class Producer(threading.Thread):
    def __init__(self, count_messages):
        super(Producer, self).__init__()
        self.count_messages = count_messages
        self.sent_messages = 0
        self.consumer = None

    def run(self) -> None:
        logging.debug("Producer start")
        while self.sent_messages < self.count_messages:
            if self.is_connected():
                message = str(self.sent_messages)
                self.send_message(message)
            time.sleep(5)
        logging.debug("Producer stop")

    def is_connected(self):
        return not self.consumer is None

    def send_message(self, message):
        while not self.is_connected():
            time.sleep(5)
        self.consumer.send_message(message)
        logging.debug("Producer sent: " + message)

        self.sent_messages += 1

    def connect(self, consumer):
        self.consumer = consumer
