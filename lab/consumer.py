import logging
import queue
import threading

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s', )


class Consumer(threading.Thread):
    def __init__(self, count_messages):
        super(Consumer, self).__init__()
        self.count_messages = count_messages
        self.received_messages = 0
        self.queue = queue.SimpleQueue()

    def run(self) -> None:
        logging.debug("Consumer start")
        while self.received_messages < self.count_messages:
            if not self.queue.empty():
                message = self.queue.get()
                logging.debug("Consumer received: " + message)
                self.received_messages += 1
            # time.sleep(5)
        logging.debug("Consumer stop")

    def send_message(self, message):
        self.queue.put(message)
